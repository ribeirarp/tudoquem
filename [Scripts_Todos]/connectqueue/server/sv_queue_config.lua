Config = {}

-- priority list can be any identifier. (hex steamid, steamid32, ip) Integer = power over other people with priority
-- a lot of the steamid converting websites are broken rn and give you the wrong steamid. I use https://steamid.xyz/ with no problems.
-- you can also give priority through the API, read the examples/readme.
Config.Priority = {
    ["steam:11000013cb88020"] = 1,
    ["steam:11000013ff1a00b"] = 1,
    ["steam:110000104f71595"] = 1,
    ["steam:11000010a2e846f"] = 1,
    ["steam:11000010a2e846f"] = 1,
    ["steam:110000117b91fb9"] = 1,
    ["steam:110000104f71595"] = 1,
    ["steam:11000010a2e846f"] = 1,
    ["steam:11000010c5780a6"] = 1,
    ["steam:1100001144cf388"] = 1,
    ["steam:110000115c5666c"] = 1,
    ["steam:110000115c812c4"] = 1,
  
    ["steam:110000100cbd146"] = 1,
    ["steam:11000010289ef7f"] = 1,
    ["steam:110000105009878"] = 1,
    ["steam:110000102a7c64f"] = 1, 
   
 
   
    ["steam:1100001085ea009"] = 1,
    ["steam:11000010b035c2b"] = 1,
    ["steam:11000011d61a2fb"] = 1,
    ["steam:11000010f3f1f5c"] = 1,
    ["steam:11000013395fcb4"] = 1,
    ["steam:11000010be2a52a"] = 1,
    ["steam:110000110610ed4"] = 1,
    ["steam:1100001112c6ddc"] = 1,
    ["steam:11000010a0a97f1"] = 1,
    ["steam:110000106cb8fd5"] = 1,
    ["steam:11000010ec700db"] = 1,
    ["steam:11000011b8f27a5"] = 1,
    ["steam:1100001046b769c"] = 1,
    ["steam:110000132c24147"] = 1
    

    
}

-- require people to run steam
Config.RequireSteam = true

-- "whitelist" only server
Config.PriorityOnly = false

-- disables hardcap, should keep this true
Config.DisableHardCap = true

-- will remove players from connecting if they don't load within: __ seconds; May need to increase this if you have a lot of downloads.
-- i have yet to find an easy way to determine whether they are still connecting and downloading content or are hanging in the loadscreen.
-- This may cause session provider errors if it is too low because the removed player may still be connecting, and will let the next person through...
-- even if the server is full. 10 minutes should be enough
Config.ConnectTimeOut = 1200

-- will remove players from queue if the server doesn't recieve a message from them within: __ seconds
Config.QueueTimeOut = 120

-- will give players temporary priority when they disconnect and when they start loading in
Config.EnableGrace = true

-- how much priority power grace time will give
Config.GracePower = 2

-- how long grace time lasts in seconds
Config.GraceTime = 480

-- on resource start, players can join the queue but will not let them join for __ milliseconds
-- this will let the queue settle and lets other resources finish initializing
Config.JoinDelay = 30000

-- will show how many people have temporary priority in the connection message
Config.ShowTemp = false

-- simple localization
Config.Language = {
    joining = "\xF0\x9F\x8E\x89A entrar...",
    connecting = "\xE2\x8F\xB3A conectar...",
    idrr = "\xE2\x9D\x97[Queue] Erro: Não consegui ver o teu id, tentar dar restart ao fivem!",
    err = "\xE2\x9D\x97[Queue] Houve um erro.",
    pos = "\xF0\x9F\x90\x8CEstás %d/%d na queue \xF0\x9F\x95\x9C%s",
    connectingerr = "\xE2\x9D\x97[Queue] Erro: Não consegui te adicionar à lista de conecções",
    timedout = "\xE2\x9D\x97[Queue] Erro: Talvez tenhas sido time out",
    wlonly = "\xE2\x9D\x97[Queue] Precisas de estar whitelist para jogar neste server!",
    steam = "\xE2\x9D\x97 [Queue] Erro: A steam tem de estar aberta!"
}