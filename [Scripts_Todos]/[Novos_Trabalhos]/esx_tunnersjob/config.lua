Config                            = {}

Config.DrawDistance               = 100.0
Config.MarkerType                 = 27
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
Config.MarkerColor                = { r = 50, g = 50, b = 204 }

Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- enable if you're using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = false -- enable if you're using esx_license

Config.EnableHandcuffTimer        = true -- enable handcuff timer? will unrestrain player after the time ends
Config.HandcuffTimer              = 10 * 60000 -- 10 mins

Config.EnableJobBlip              = false -- enable blips for colleagues, requires esx_society

Config.MaxInService               = -1
Config.Locale                     = 'br'

Config.tunnersStations = {

  tunners1 = {

    Blip = {
      Pos     = { x = 425.130, y = -979.558, z = 30.711 },
      Sprite  = 60,
      Display = 4,
      Scale   = 1.2,
      Colour  = 29,
    },

    AuthorizedWeapons = {
      { name = 'WEAPON_FLASHLIGHT',       price = 200 },	
      { name = 'WEAPON_NIGHTSTICK',       price = 400 },	
      { name = 'WEAPON_STUNGUN',          price = 10000 },	
	  { name = 'WEAPON_COMBATPISTOL',     price = 30000 }, 	
      { name = 'WEAPON_PUMPSHOTGUN',      price = 60000 }, 	
  	  { name = 'WEAPON_SMG',              price = 65000 },
      { name = 'WEAPON_CARBINERIFLE',     price = 90000 },
	  { name = 'WEAPON_SMOKEGRENADE',     price = 300 },
	  { name = 'WEAPON_BZGAS',            price = 300 },
	  { name = 'GADGET_PARACHUTE',        price = 500 },

    },

    Cloakrooms = {
      { x = 76.120, y =  -1961.66, z = 20.75-0.98 },    
    },

    Armories = {
     { x = 81.93, y = -1964.322, z = 17.83-0.98 },    
    },

    Vehicles = {
      {
        Spawner     = { x =  101.20, y =  -1954.65, z = 20.71-0.98 },      
        SpawnPoints = { 
			{  x = 104.36, y =  -1955.71, z = 20.74, heading = 0.97, radius = 3 }  
		}
      },

	},
	
    Helicopters = {
      {
       Spawner    = {x = 466.477, y = -982.819, z = 42.695-0.98},
        SpawnPoint = {x = 450.04, y = -981.14, z = 42.695-0.98},
		Heading    = 0.0
      }
    },

    VehicleDeleters = { 
       { x = 104.36, y =  -1955.71, z = 20.74-0.98 },  
  
    },

    BossActions = {
      { x = 69.01, y =  -1971.16, z = 20.83-0.98 }     
    }

  }

}

-- https://wiki.rage.mp/index.php?title=Vehicles
Config.AuthorizedVehicles = {

    Shared = {

		{
			model = 'diavel',
			label = 'Ducati Diavel'
		},
		{
			model = 'm3e46',
			label = 'BMW M3 E46'
		},
		{
			model = 'xclass',
			label = 'Mercedes Classe X'
		},
	
	
	
	},
	
	recruit = {
		

	},

	officer = {
		

	},

	sergeant = {
		
		

	},

	lieutenant = {
		

	},
	
	boss = {
		
		
	}
}


-- CHECK SKINCHANGER CLIENT MAIN.LUA for matching elements

Config.Uniforms = {
	chefe1_wear = {
		male = {
			tshirt_1 = 4,
			tshirt_2 = 0,
			torso_1 = 72,
			torso_2 = 2,
			arms = 4,
			pants_1 = 24,
			pants_2 = 0,
			shoes_1 = 3,
			shoes_2 = 4,
			chain_1 = 24,
			chain_2 = 9,
			helmet_1 = -1,
			glasses_1 = 5,
			glasses_2 = 0
		},
		female = {
			torso_1          = 146,
            torso_2         = 2,
            tshirt_1         = 15,
			tshirt_2         = 0,
            arms             = 1,
            pants_1          = 78,
            pants_2         = 1,
		}
	},
	
	chefe2_wear = {
		male = {			
			tshirt_1 = 4,
			tshirt_2 = 0,
			torso_1 = 142,
			torso_2 = 0,
			arms = 	4,
			pants_1 = 24,
			pants_2 = 0,
			shoes_1 = 3,	
			shoes_2 = 10,
			chain_1 = 22,
			chain_2 = 8,
			helmet_1 = 61,
			helmet_2 = 2
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	casual1_wear = {
		male = {	
			tshirt_1 = 76,
			tshirt_2 = 0,
			torso_1 = 127,
			torso_2 = 3,
			arms = 	4,
			pants_1 = 15,
			pants_2 = 3,
			shoes_1 = 6,	
			shoes_2 = 0,
			chain_1 = 0,
			chain_2 = 0,
			helmet_1 = 3,
			helmet_2 = 1

		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	
	casual2_wear = {
		male = {			
			tshirt_1 = 15,
			tshirt_2 = 0,
			torso_1 = 84,
			torso_2 = 5,
			arms = 	37,
			pants_1 = 42,
			pants_2 = 0,
			shoes_1 = 8,	
			shoes_2 = 0,
			chain_1 = 17,
			chain_2 = 0,
			helmet_1 = -1,
			helmet_2 = 0


		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	casual3_wear = {
		male = {			
			
			tshirt_1 = 15,
			tshirt_2 = 0,
			torso_1 = 84,
			torso_2 = 2,
			arms = 	38,
			pants_1 = 42,
			pants_2 = 0,
			shoes_1 = 8,	
			shoes_2 = 0,
			chain_1 = 17,
			chain_2 = 0,
			helmet_1 = -1,
			helmet_2 = 0



		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	casual4_wear = {
		male = {			
			
						
			tshirt_1 = 15,
			tshirt_2 = 0,
			torso_1 = 7,
			torso_2 = 9,
			arms = 	39,
			pants_1 = 5,
			pants_2 = 9,
			shoes_1 = 8,	
			shoes_2 = 0,
			chain_1 = 17,
			chain_2 = 0,
			helmet_1 = -1,
			helmet_2 = 0




		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	casual5_wear = {
		male = {			
			
						
			tshirt_1 = 15,
			tshirt_2 = 0,
			torso_1 = 82,
			torso_2 = 14,
			arms = 	37,
			pants_1 = 5,
			pants_2 = 9,
			shoes_1 = 8,	
			shoes_2 = 0,
			chain_1 = 17,
			chain_2 = 0,
			helmet_1 = -1,
			helmet_2 = 0





		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	assalto_wear = {
		male = {			
					
			tshirt_1 = 55,
			tshirt_2 = 0,
			torso_1 = 49,
			torso_2 = 0,
			arms = 	38,
			pants_1 = 97,
			pants_2 = 1,
			shoes_1 = 25,	
			shoes_2 = 0,
			chain_1 = 17,
			chain_2 = 0,
			helmet_1 = -1,
			helmet_2 = 0,
			bags_1 = 37
			

		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	major_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 3,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 3,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	boss_wear = { -- currently the same as chef_wear
		male = {
			['tshirt_1'] = 130,  ['tshirt_2'] = 0,
			['torso_1'] = 53,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 11,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = 106,  ['helmet_2'] = 20,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	bullet_wear = {
		male = {
			['bproof_1'] = 11,  ['bproof_2'] = 0
		},
		female = {
			['bproof_1'] = 13,  ['bproof_2'] = 1
		}
	},
	gilet_wear = {
		male = {
			['tshirt_1'] = 59,  ['tshirt_2'] = 1
		},
		female = {
			['tshirt_1'] = 36,  ['tshirt_2'] = 1
		}
	}
}