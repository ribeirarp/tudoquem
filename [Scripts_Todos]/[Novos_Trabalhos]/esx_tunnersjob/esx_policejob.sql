INSERT INTO `addon_account` (name, label, shared) VALUES
	('society_tunners', 'tunners', 1)
;

INSERT INTO `datastore` (name, label, shared) VALUES
	('society_tunners', 'tunners', 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
	('society_tunners', 'tunners', 1)
;

INSERT INTO `jobs` (name, label) VALUES
	('tunners','Loyalty Tunners')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
	('tunners',0,'recruit',			'Aprendiz',450,'{}','{}'),
	('tunners',1,'officer',			'Tunner',450,'{}','{}'),
	('tunners',2,'sergeant',		'Bra�o esquerdo',450,'{}','{}'),
	('tunners',3,'lieutenant',		'Bra�o direito',450,'{}','{}'),
	('tunners',4,'boss',				'Chefe',450,'{}','{}')
;



