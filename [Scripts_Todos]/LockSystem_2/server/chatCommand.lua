if(globalConf["SERVER"].enableGiveKey)then
    RegisterCommand('givekey', function(source, args, rawCommand)
        local src = source
        local identifier = GetPlayerIdentifiers(src)[1]

        if(args[1])then
            local targetId = args[1]
            local targetIdentifier = GetPlayerIdentifiers(targetId)[1]
            if(targetIdentifier)then
                if(targetIdentifier ~= identifier)then
                    if(args[2])then
                        local plate = string.lower(args[2])
                        if(owners[plate])then
                            if(owners[plate] == identifier)then
                                alreadyHas = false
                                for k, v in pairs(secondOwners) do
                                    if(k == plate)then
                                        for _, val in ipairs(v) do
                                            if(val == targetIdentifier)then
                                                alreadyHas = true
                                            end
                                        end
                                    end
                                end

                                if(not alreadyHas)then
                                    TriggerClientEvent("ls:giveKeys", targetIdentifier, plate)
                                    TriggerEvent("ls:addSecondOwner", targetIdentifier, plate)

                                    TriggerClientEvent("ls:notify", targetId, "Recebeu as chaves para o veiculo " .. plate .. " pelo " .. GetPlayerName(src))
                                    TriggerClientEvent("ls:notify", src, "Tu deste as chaves do veiculo " .. plate .. " a " .. GetPlayerName(targetId))
                                else
                                    TriggerClientEvent("ls:notify", src, "Esta pessoa já tem essas changes")
                                    TriggerClientEvent("ls:notify", targetId, GetPlayerName(src) .. " tentou dar-te as chaves mas tu já tinhas")
                                end
                            else
                                TriggerClientEvent("ls:notify", src, "Este veiculo não é teu")
                            end
                        else
                            TriggerClientEvent("ls:notify", src, "The vehicle with this plate doesn't exist")
                        end
                    else
                        TriggerClientEvent("ls:notify", src, "Second missing argument : /givekey <id> <plate>")
                    end
                else
                    TriggerClientEvent("ls:notify", src, "Não podes dar a ti proprio")
                end
            else
                TriggerClientEvent("ls:notify", src, "Player not found")
            end
        else
            TriggerClientEvent("ls:notify", src, 'First missing argument : /givekey <id> <plate>')
        end

        CancelEvent()
    end)
end
