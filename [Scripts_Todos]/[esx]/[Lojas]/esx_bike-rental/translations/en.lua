Locales['en'] = {
	['press_e'] = 'clica ~INPUT_CONTEXT~ para alugares uma bicicleta.',
	['storebike'] = 'Clica ~INPUT_CONTEXT~ para guardares a bicicleta.',	
	['biketitle'] = 'Aluguer de bicicletas',
	['bike'] = 'Bike - <span style="color:green;">TriBike</span> <span style="color:red;">320$</span>',
	['bike2'] = 'Bike - <span style="color:green;">Scorcher</span> <span style="color:red;">100$</span>',
	['bike3'] = 'Bike - <span style="color:green;">Cruiser</span> <span style="color:red;">150$</span>',
	['bike4'] = 'Bike - <span style="color:green;">BMX</span> <span style="color:red;">300$</span>',
	['bikefree'] = 'Bike - <span style="color:green;">TriBike</span>',
	['bike2free'] = 'Bike - <span style="color:green;">Scorcher</span>',
	['bike3free'] = 'Bike - <span style="color:green;">Cruiser</span>',
	['bike4free'] = 'Bike - <span style="color:green;">BMX</span>',
	['bikemessage'] = 'Espero que tenhas gostado da viagem! Volta mais logo :)',
	['notabike'] = 'Nao estas numa bicicleta!',
	
	['bike_pay'] = 'Tu pagas-te: $%s',
	
	['bikes'] = '[Bikes]',
	
	['civil_outfit'] = '<span style="color:yellow;">Citizen Outfit</span>',
	['outfit'] = '<span style="color:green;">Bike Outfit</span> <span style="color:red;">200$</span>',
	['outfit2'] = '<span style="color:green;">Bike Outfit 2</span> <span style="color:red;">200$</span>',
	['outfit3'] = '<span style="color:green;">Bike Outfit 3</span> <span style="color:red;">200$</span>',
}
