Config                            = {}
Config.Locale = 'en'


Config.Volume = 0.5 				-- 0.1 , 1.0
Config.EnablePrice = true -- false = bikes for free
Config.EnableEffects = true
Config.EnableSoundEffects = false
Config.EnableBlips = true

	
Config.PriceTriBike = 320
Config.PriceScorcher = 100
Config.PriceCruiser = 150
Config.PriceBmx = 300

Config.EnableBuyOutfits = false -- WIP !!!!
	
Config.MarkerZones = { 

    {x = -246.980,y = -339.820,z = 29.000},
    {x = -6.986,y = -1081.704,z = 25.7},
    {x = -1085.78,y = -263.01,z = 36.80}, 
    {x = -1262.36,y = -1438.98,z = 3.45},
    {x = -212.100,y = 6348.064,z = 31.000},

}


-- Edit blip titles
Config.BlipZones = { 

   {title="Aluguer de bicicletas", colour=2, id=226, x = -248.938, y = -339.955, z = 29.969},
   {title="Aluguer de bicicletas", colour=2, id=226, x = -6.892, y = -1081.734, z = 26.829},
   {title="Aluguer de bicicletas", colour=2, id=226, x = -1262.36, y = -1438.98, z = 3.45},
   {title="Aluguer de bicicletas", colour=2, id=226, x = -212.100, y = 6348.064, z = 31.000},
}
