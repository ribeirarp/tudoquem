Config = {}

Config.Debug = false

Config.AlignMenu = "right" -- this is where the menu is located [left, right, center, top-right, top-left etc.]

Config.StoreBlips = true -- enable blips on every store.

Config.BlackMoney = false -- if you want to purchase with black money.

Config.DefaultAmmo = 120 -- this is the ammo you will get when purchasing a weapon.

Config.MarkerData = {
    ["type"] = 6,
    ["size"] = vector3(1.5, 1.5, 1.5),
    ["color"] = vector3(0, 255, 150),
    ["range"] = 5.0
}

Config.WeaponShops = {
    {
        ["name"] = "Ammu Nation",
        ["clerkMarker"] = {
            ["position"] = vector3(20.174058914185, -1106.2066650391, 29.797029495239),
            ["heading"] = 340.0
        },
        ["clerk"] = {
            ["model"] = 0x9E08633D,
            ["position"] = vector3(20.823081970215, -1104.3601074219, 29.797029495239),
            ["heading"] = 160.0
        },
        ["camera"] = {
            ["x"] = 21.343982696533, 
            ["y"] = -1107.1812744141, 
            ["z"] = 30.963396072388, 
            ["rotationX"] = -26.204723984003, 
            ["rotationY"] = 0.0, 
            ["rotationZ"] = -110.33070892096
        },
        ["object"] = {
            ["position"] = vector3(23.115102767944, -1107.7210693359, 30.272029876709)
        }
    }
}

Config.Weapons = {
    {
        ["model"] = "WEAPON_PISTOL",
        ["price"] = 19000
    }
}

-- These are the prices for each ammo.
Config.AmmoTypes = {
    [`AMMO_PISTOL`] = 50
}

Config.WeaponComponents = {
    ["WEAPON_PISTOL"] = {
        ["mag"] = {
            {
                ["component"] = "COMPONENT_PISTOL_CLIP_02",
                ["price"] = 2500
            }
        },
        ["suppressor"] = {
            {
                ["component"] = "COMPONENT_AT_PI_SUPP_02",
                ["price"] = 2900
            }
        },
        ["flashlight"] = {
            {
                ["component"] = "COMPONENT_AT_PI_FLSH",
                ["price"] = 750
            }
        }
    }
}

Config.DefaultWeapon = Config.Weapons[1]

Config.UnderDev = {
     ["WEAPON_PISTOL"] = {"COMPONENT_PISTOL_CLIP_02", "COMPONENT_AT_PI_SUPP_02", "COMPONENT_AT_PI_FLSH"},
}