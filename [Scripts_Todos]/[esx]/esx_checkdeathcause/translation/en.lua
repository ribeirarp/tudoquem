Locales['en'] = {
  -- regulars
  	['press_e'] = 'Pressiona ~INPUT_CONTEXT~ para ver o porquê da ~r~morte~s~ da pessoa.',
	['hardmeele'] = 'Foi atingido com força na cabeça.',
	['bullet'] = 'Buracos de bala pelo corpo todo.',
	['knifes'] = 'Provavelmente foi esfaqueado.',
	['bitten'] = 'Provavelmente foi mordido por algum animal.',
	['brokenlegs'] = 'Provavelmente caiu, tem as duas pernas partidas.',
	['explosive'] = 'Provavelmente morreu por algum tipo de explosivo.',
	['gas'] = 'Provavelmente morreu com gás nos pulmões.',
	['fireextinguisher'] = 'Provavelmente morreu com gás do extintor do incêndio nos pulmões.',
	['fire'] = 'Morreu com queimaduras.',
	['caraccident'] = 'Provavelmente morreu num acidente de carro.',
	['drown'] = 'Provavelmente morreu afogado.',
	['unknown'] = 'Causa da morte desconhecida.',
}
