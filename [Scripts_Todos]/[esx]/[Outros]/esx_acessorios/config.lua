Config = {}

Config.DrawDistance = 100
Config.Size         = {x = 1.5, y = 1.5, z = 0.3}
Config.Color        = {r = 0, g = 128, b = 255}
Config.Type         = 27
Config.Locale = 'br'

Config.Zones = {

    LojadeAcessorios = {
        Items = {},
        Pos = {
            {x = 458.44,   y = -979.64,  z = 30.73-1.0001}
        }
    }
}
