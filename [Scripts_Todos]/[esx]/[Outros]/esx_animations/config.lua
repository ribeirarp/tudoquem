Config = {}
 
Config.Animations = {
   
    {
        name  = 'Festa',
        label = 'Festa',
        items = {
        {label = "Tocar um Instrumento", type = "scenario", data = {anim = "WORLD_HUMAN_MUSICIAN"}},
        {label = "Dj", type = "anim", data = {lib = "anim@mp_player_intcelebrationmale@dj", anim = "dj"}},
        {label = "Beber Refrigerante", type = "scenario", data = {anim = "WORLD_HUMAN_DRINKING"}},
        {label = "Beber Cerveja", type = "scenario", data = {anim = "WORLD_HUMAN_PARTYING"}},
        {label = "Guitarra", type = "anim", data = {lib = "anim@mp_player_intcelebrationmale@air_guitar", anim = "air_guitar"}},
        {label = "Sarrada", type = "anim", data = {lib = "anim@mp_player_intcelebrationfemale@air_shagging", anim = "air_shagging"}},
        {label = "Rock'n'roll", type = "anim", data = {lib = "mp_player_int_upperrock", anim = "mp_player_int_rock"}},
        -- {label = "Fumer un joint", type = "scenario", data = {anim = "WORLD_HUMAN_SMOKING_POT"}},
        {label = "Bebado", type = "anim", data = {lib = "amb@world_human_bum_standing@drunk@idle_a", anim = "idle_a"}},
        {label = "Vomitar no Carro", type = "anim", data = {lib = "oddjobs@taxi@tie", anim = "vomit_outside"}},
        }
    },
 
    {
        name  = 'Comprimentar',
        label = 'Comprimentar',
        items = {
        {label = "Hey", type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_hello"}},
        {label = "Aperto de Mão", type = "anim", data = {lib = "mp_common", anim = "givetake1_a"}},
        {label = "Toca Ae", type = "anim", data = {lib = "mp_ped_interaction", anim = "handshake_guy_a"}},
        {label = "Dae Mano ", type = "anim", data = {lib = "mp_ped_interaction", anim = "hugs_guy_a"}},
        {label = "Continência", type = "anim", data = {lib = "mp_player_int_uppersalute", anim = "mp_player_int_salute"}},
        }
    },
 
    {
        name  = 'Trabalho',
        label = 'Trabalho',
        items = {
        {label = "Render-se", type = "anim", data = {lib = "random@arrests@busted", anim = "idle_c"}},
        {label = "Pescar", type = "scenario", data = {anim = "world_human_stand_fishing"}},
        {label = "Investigar o Chão", type = "anim", data = {lib = "amb@code_human_police_investigate@idle_b", anim = "idle_f"}},
        {label = "Chamar no Rádio", type = "anim", data = {lib = "random@arrests", anim = "generic_radio_chatter"}},
        {label = "Controlar Tráfego", type = "scenario", data = {anim = "WORLD_HUMAN_CAR_PARK_ATTENDANT"}},
        {label = "Binoculos", type = "scenario", data = {anim = "WORLD_HUMAN_BINOCULARS"}},
        {label = "Cavar o Chão", type = "scenario", data = {anim = "world_human_gardener_plant"}},
        {label = "Consertar Deitado", type = "scenario", data = {anim = "world_human_vehicle_mechanic"}},
        {label = "Reparar Motor", type = "anim", data = {lib = "mini@repair", anim = "fixing_a_ped"}},
        {label = "Examinar", type = "scenario", data = {anim = "CODE_HUMAN_MEDIC_KNEEL"}},
        {label = "Conversar com Cliente", type = "anim", data = {lib = "oddjobs@taxi@driver", anim = "leanover_idle"}},
        {label = "Entregar CNH", type = "anim", data = {lib = "oddjobs@taxi@cyi", anim = "std_hand_off_ps_passenger"}},
        {label = "Carregar Porta-Malas", type = "anim", data = {lib = "mp_am_hold_up", anim = "purchase_beerbox_shopkeeper"}},
        {label = "Servir Drink", type = "anim", data = {lib = "mini@drinking", anim = "shots_barman_b"}},
        {label = "Tirar uma Foto", type = "scenario", data = {anim = "WORLD_HUMAN_PAPARAZZI"}},
        {label = "Pegar Prancheta", type = "scenario", data = {anim = "WORLD_HUMAN_CLIPBOARD"}},
        {label = "Martelar a Parede", type = "scenario", data = {anim = "WORLD_HUMAN_HAMMERING"}},
        {label = "Segurar Placa", type = "scenario", data = {anim = "WORLD_HUMAN_BUM_FREEWAY"}},
        {label = "Estátua Humana", type = "scenario", data = {anim = "WORLD_HUMAN_HUMAN_STATUE"}},
        }
    },
 
    {
        name  = 'Ação',
        label = 'Ação',
        items = {
        {label = "Bater Palma", type = "scenario", data = {anim = "WORLD_HUMAN_CHEERING"}},
        {label = "Blz", type = "anim", data = {lib = "mp_action", anim = "thanks_male_06"}},
        {label = "Apontar", type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_point"}},
        {label = "Chamar", type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_come_here_soft"}},
        {label = "Que é Isso!", type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_bring_it_on"}},
        {label = "Eu", type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_me"}},
        {label = "Roubar", type = "anim", data = {lib = "anim@am_hold_up@male", anim = "shoplift_high"}},
        {label = "Decepcionado", type = "anim", data = {lib = "anim@mp_player_intcelebrationmale@face_palm", anim = "face_palm"}},
        {label = "Calma Ai!", type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_easy_now"}},
        {label = "Assustado", type = "anim", data = {lib = "oddjobs@assassinate@multi@", anim = "react_big_variations_a"}},
        {label = "Medo", type = "anim", data = {lib = "amb@code_human_cower_stand@male@react_cowering", anim = "base_right"}},
        {label = "Brigar", type = "anim", data = {lib = "anim@deathmatch_intros@unarmed", anim = "intro_male_unarmed_e"}},
        {label = "Que Merda!", type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_damn"}},
        {label = "Beijar", type = "anim", data = {lib = "mp_ped_interaction", anim = "kisses_guy_a"}},
        {label = "Foda-se!", type = "anim", data = {lib = "mp_player_int_upperfinger", anim = "mp_player_int_finger_01_enter"}},
        {label = "Punheta", type = "anim", data = {lib = "mp_player_int_upperwank", anim = "mp_player_int_wank_01"}},
        {label = "Suicidio!", type = "anim", data = {lib = "mp_suicide", anim = "pistol"}},
        }
    },
 
    {
        name  = 'Esportes',
        label = 'Esportes',
        items = {
        {label = "Alongar-se", type = "anim", data = {lib = "amb@world_human_muscle_flex@arms_at_side@base", anim = "base"}},
        {label = "Levantar Peso", type = "anim", data = {lib = "amb@world_human_muscle_free_weights@male@barbell@base", anim = "base"}},
        {label = "Flexão", type = "anim", data = {lib = "amb@world_human_push_ups@male@base", anim = "base"}},
        {label = "Abdominal", type = "anim", data = {lib = "amb@world_human_sit_ups@male@base", anim = "base"}},
        {label = "Yoga", type = "anim", data = {lib = "amb@world_human_yoga@male@base", anim = "base_a"}},
        }
    },
 
    {
        name  = 'Outros',
        label = 'Outros',
        items = {
        {label = "Beber Café", type = "anim", data = {lib = "amb@world_human_aa_coffee@idle_a", anim = "idle_a"}},
        {label = "Sentar e Mexer no Celular", type = "anim", data = {lib = "anim@heists@prison_heistunfinished_biztarget_idle", anim = "target_idle"}},
        {label = "Sentar no Chão", type = "scenario", data = {anim = "WORLD_HUMAN_PICNIC"}},
        {label = "Encostar-se", type = "scenario", data = {anim = "world_human_leaning"}},
        {label = "Deitar de Costas", type = "scenario", data = {anim = "WORLD_HUMAN_SUNBATHE_BACK"}},
        {label = "Deitar de Bruço", type = "scenario", data = {anim = "WORLD_HUMAN_SUNBATHE"}},
        {label = "Limpar Vidro", type = "scenario", data = {anim = "world_human_maid_clean"}},
        {label = "Fazer Churrasco", type = "scenario", data = {anim = "PROP_HUMAN_BBQ"}},
        {label = "Titanic", type = "anim", data = {lib = "mini@prostitutes@sexlow_veh", anim = "low_car_bj_to_prop_female"}},
        {label = "Tirar uma Selfie", type = "scenario", data = {anim = "world_human_tourist_mobile"}},
        {label = "Ouvir Conversa", type = "anim", data = {lib = "mini@safe_cracking", anim = "idle_base"}},
        }
    },
	
	{
		name  = 'Atitudes',
		label = 'Atitudes',
		items = {
	    {label = "Normal M", type = "attitude", data = {lib = "move_m@confident", anim = "move_m@confident"}},
	    {label = "Normal F", type = "attitude", data = {lib = "move_f@heels@c", anim = "move_f@heels@c"}},
	    {label = "Depressivo M", type = "attitude", data = {lib = "move_m@depressed@a", anim = "move_m@depressed@a"}},
	    {label = "Depressivo F", type = "attitude", data = {lib = "move_f@depressed@a", anim = "move_f@depressed@a"}},
	    {label = "Empresário", type = "attitude", data = {lib = "move_m@business@a", anim = "move_m@business@a"}},
	    {label = "Determinado", type = "attitude", data = {lib = "move_m@brave@a", anim = "move_m@brave@a"}},
	    {label = "Casual", type = "attitude", data = {lib = "move_m@casual@a", anim = "move_m@casual@a"}},
	    {label = "Braços Abertos", type = "attitude", data = {lib = "move_m@fat@a", anim = "move_m@fat@a"}},
	    {label = "Hipster", type = "attitude", data = {lib = "move_m@hipster@a", anim = "move_m@hipster@a"}},
	    {label = "Mancando", type = "attitude", data = {lib = "move_m@injured", anim = "move_m@injured"}},
	    {label = "Intimidado", type = "attitude", data = {lib = "move_m@hurry@a", anim = "move_m@hurry@a"}},
	    {label = "Lesado", type = "attitude", data = {lib = "move_m@hobo@a", anim = "move_m@hobo@a"}},
	    {label = "Triste", type = "attitude", data = {lib = "move_m@sad@a", anim = "move_m@sad@a"}},
	    {label = "Musculoso", type = "attitude", data = {lib = "move_m@muscle@a", anim = "move_m@muscle@a"}},
	    {label = "Largado", type = "attitude", data = {lib = "move_m@shocked@a", anim = "move_m@shocked@a"}},
	    {label = "Malandro", type = "attitude", data = {lib = "move_m@shadyped@a", anim = "move_m@shadyped@a"}},
	    {label = "Cansado", type = "attitude", data = {lib = "move_m@buzzed", anim = "move_m@buzzed"}},
	    {label = "Apressado", type = "attitude", data = {lib = "move_m@hurry_butch@a", anim = "move_m@hurry_butch@a"}},
	    {label = "Desfilar", type = "attitude", data = {lib = "move_m@money", anim = "move_m@money"}},
	    {label = "Rapidinho", type = "attitude", data = {lib = "move_m@quick", anim = "move_m@quick"}},
	    {label = "Rebolar", type = "attitude", data = {lib = "move_f@maneater", anim = "move_f@maneater"}},
	    {label = "Atrevido", type = "attitude", data = {lib = "move_f@sassy", anim = "move_f@sassy"}},	
	    {label = "Delicado", type = "attitude", data = {lib = "move_f@arrogant@a", anim = "move_f@arrogant@a"}},
		}
	},

    {
        name  = '+18',
        label = '+18',
        items = {
        {label = "Carro - Ganhar Boquete", type = "anim", data = {lib = "oddjobs@towing", anim = "m_blow_job_loop"}},
        {label = "Carro - Fazer Boquete", type = "anim", data = {lib = "oddjobs@towing", anim = "f_blow_job_loop"}},
        {label = "Carro - Sexo M", type = "anim", data = {lib = "mini@prostitutes@sexlow_veh", anim = "low_car_sex_loop_player"}},
        {label = "Carro - Sexo F", type = "anim", data = {lib = "mini@prostitutes@sexlow_veh", anim = "low_car_sex_loop_female"}},
        {label = "Coçar as Bollas", type = "anim", data = {lib = "mp_player_int_uppergrab_crotch", anim = "mp_player_int_grab_crotch"}},
        {label = "Insinuar-se", type = "anim", data = {lib = "mini@strip_club@idles@stripper", anim = "stripper_idle_02"}},
        {label = "Mão na Cintura", type = "scenario", data = {anim = "WORLD_HUMAN_PROSTITUTE_HIGH_CLASS"}},
        {label = "Balançar os Peitos", type = "anim", data = {lib = "mini@strip_club@backroom@", anim = "stripper_b_backroom_idle_b"}},
        {label = "Strip Tease 1", type = "anim", data = {lib = "mini@strip_club@lap_dance@ld_girl_a_song_a_p1", anim = "ld_girl_a_song_a_p1_f"}},
        {label = "Strip Tease 2", type = "anim", data = {lib = "mini@strip_club@private_dance@part2", anim = "priv_dance_p2"}},
        {label = "Stip Tease 3", type = "anim", data = {lib = "mini@strip_club@private_dance@part3", anim = "priv_dance_p3"}},
        }
    },
 
}