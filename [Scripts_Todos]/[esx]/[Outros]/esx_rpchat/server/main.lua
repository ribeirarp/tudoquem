AddEventHandler('es:invalidCommandHandler', function(source, command_args, user)
	CancelEvent()
	TriggerClientEvent('chat:addMessage', source, { args = { '^1SYSTEM', _U('unknown_command', command_args[1]) } })
end)



AddEventHandler('chatMessage', function(source, name, message)
	if string.sub(message, 1, string.len('/')) ~= '/' then
		CancelEvent()

		--if Config.EnableESXIdentity then name = GetCharacterName(source) end
		TriggerClientEvent('chat:addMessage', -1, { args = { _U('ooc_prefix', name), message }, color = { 128, 128, 128 } })
	end
end)

RegisterCommand('twt', function(source, args, rawCommand)
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end


	args = table.concat(args, ' ')
	TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "twt", GetPlayerIdentifiers(source)[1])
	local name = GetPlayerName(source)
	if Config.EnableESXIdentity then name = GetCharacterNameESX(source) end
	
	TriggerClientEvent('chat:addMessage', -1, { args = { _U('twt_prefix', name), args }, color = { 0, 153, 2048 } })
end, false)

RegisterCommand('ooc', function(source, args, rawCommand)
	if source == 0 then
		print('esx_rpchat: you can\'t use this command from rcon!')
		return
	end

	args = table.concat(args, ' ')
	TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args, "ooc", GetPlayerIdentifiers(source)[1])
	local name = GetPlayerName(source)
	if Config.EnableESXIdentity then name = GetPlayerName(source) end
	TriggerClientEvent('chat:addMessage', -1, { args = { _U('ooc_prefix', name), args }, color = { 128, 128, 128 } })
end, false)

RegisterCommand('anon', function(source, args, rawCommand)
    args1 = table.concat(args, ' ')

	TriggerEvent("esx_discord_bot:mandar", "[ " .. source .. " ]  " .. GetPlayerName(source), GetPlayerIdentifiers(source)[1], args1, "anon", GetPlayerIdentifiers(source)[1])
	--TriggerEvent("esx_discord_bot:mandar", GetPlayerName(source), 2, args1, "anon", GetPlayerIdentifiers(source)[1])
	TriggerClientEvent('chat:addMessage', -1, {
       template = '<div><b>[^1Anonimo^0]:</b> {0}</div>',
        args = { args1 } 
    })
end, false)



function GetCharacterNameESX(source)
	local result = MySQL.Sync.fetchAll('SELECT firstname, lastname FROM users WHERE identifier = @identifier', {
		['@identifier'] = GetPlayerIdentifiers(source)[1]
	})

	if result[1] and result[1].firstname and result[1].lastname then
		if Config.OnlyFirstname then
			return result[1].firstname
		else
			return ('%s %s'):format(result[1].firstname, result[1].lastname)
		end
	else
		return GetPlayerName(source)
	end
end