Config = {}
Config.Locale = 'en'

Config.DoorList = {

	--
	-- Mission Row First Floor
	--

	-- Entrance Doors
	{
		textCoords = vector3(434.7, -982.0, 31.5),
		authorizedJobs = { 'police', 'pj' },
		locked = false,
		distance = 2.5,
		doors = {
			{
				objName = 'v_ilev_ph_door01',
				objYaw = -90.0,
				objCoords = vector3(434.7, -980.6, 30.8)
			},

			{
				objName = 'v_ilev_ph_door002',
				objYaw = -90.0,
				objCoords = vector3(434.7, -983.2, 30.8)
			}
		}
	},

	-- To locker room & roof
	{
		objName = 'v_ilev_ph_gendoor004',
		objYaw = 90.0,
		objCoords  = vector3(449.6, -986.4, 30.6),
		textCoords = vector3(450.1, -986.3, 31.7),
		authorizedJobs = { 'police', 'pj' },
		locked = true
	},

	-- Rooftop
	{
		objName = 'v_ilev_gtdoor02',
		objYaw = 90.0,
		objCoords  = vector3(464.3, -984.6, 43.8),
		textCoords = vector3(464.3, -984.0, 44.8),
		authorizedJobs = { 'police', 'pj' },
		locked = true
	},

	-- Hallway to roof
	{
		objName = 'v_ilev_arm_secdoor',
		objYaw = 90.0,
		objCoords  = vector3(461.2, -985.3, 30.8),
		textCoords = vector3(461.5, -986.0, 31.5),
		authorizedJobs = { 'police', 'pj' },
		locked = true
	},

	-- Captain Office
	{
		objName = 'v_ilev_ph_gendoor002',
		objYaw = -180.0,
		objCoords  = vector3(447.2, -980.6, 30.6),
		textCoords = vector3(447.2, -980.0, 31.7),
		authorizedJobs = { 'police', 'pj' },
		locked = true
	},

	-- To downstairs (double doors)
	{
		textCoords = vector3(444.6, -989.4, 31.7),
		authorizedJobs = { 'police', 'pj' },
		locked = true,
		distance = 4,
		doors = {
			{
				objName = 'v_ilev_ph_gendoor005',
				objYaw = 180.0,
				objCoords = vector3(443.9, -989.0, 30.6)
			},

			{
				objName = 'v_ilev_ph_gendoor005',
				objYaw = 0.0,
				objCoords = vector3(445.3, -988.7, 30.6)
			}
		}
	},

	--
	-- Mission Row Cells
	--

	-- Main Cells
	{
		objName = 'v_ilev_ph_cellgate',
		objYaw = 0.0,
		objCoords  = vector3(463.8, -992.6, 24.9),
		textCoords = vector3(463.96, -992.70, 24.91),
		authorizedJobs = { 'police', 'pj' },
		locked = true
	},

	-- Cell 1
	{
		objName = 'v_ilev_ph_cellgate',
		objYaw = -90.0,
		objCoords  = vector3(462.3, -993.6, 24.9),
		textCoords = vector3(461.8, -993.3, 25.0),
		authorizedJobs = { 'police', 'pj' },
		locked = true
	},

	-- Cell 2
	{
		objName = 'v_ilev_ph_cellgate',
		objYaw = 90.0,
		objCoords  = vector3(462.3, -998.1, 24.9),
		textCoords = vector3(461.8, -998.8, 25.0),
		authorizedJobs = { 'police', 'pj' },
		locked = true
	},

	-- Cell 3
	{
		objName = 'v_ilev_ph_cellgate',
		objYaw = 90.0,
		objCoords  = vector3(462.7, -1001.9, 24.9),
		textCoords = vector3(461.8, -1002.4, 25.0),
		authorizedJobs = { 'police', 'pj' },
		locked = true
	},

	-- To Back
	{
		objName = 'v_ilev_gtdoor',
		objYaw = 0.0,
		objCoords  = vector3(463.4, -1003.5, 25.0),
		textCoords = vector3(464.0, -1003.5, 25.5),
		authorizedJobs = { 'police', 'pj' },
		locked = true
	},

	--
	-- Mission Row Back
	--

	-- Back (double doors)
	{
		textCoords = vector3(468.6, -1014.4, 27.1),
		authorizedJobs = { 'police', 'pj' },
		locked = true,
		distance = 4,
		doors = {
			{
				objName = 'v_ilev_rc_door2',
				objYaw = 0.0,
				objCoords  = vector3(467.3, -1014.4, 26.5)
			},

			{
				objName = 'v_ilev_rc_door2',
				objYaw = 180.0,
				objCoords  = vector3(469.9, -1014.4, 26.5)
			}
		}
	},

	-- Back Gate
	{
		objName = 'hei_prop_station_gate',
		objYaw = 90.0,
		objCoords  = vector3(488.8, -1017.2, 27.1),
		textCoords = vector3(488.8, -1020.2, 30.0),
		authorizedJobs = { 'police', 'pj' },
		locked = true,
		distance = 14,
		size = 2
	},

	--
	-- Sandy Shores
	--

	-- Entrance
	{
		objName = 'v_ilev_shrfdoor',
		objYaw = 30.0,
		objCoords  = vector3(1855.1, 3683.5, 34.2),
		textCoords = vector3(1855.1, 3683.5, 35.0),
		authorizedJobs = { 'police', 'pj' },
		locked = false
	},
	-- Novas
	{
		textCoords = vector3(445.91, -999.028, 30.72),
		authorizedJobs = { 'police', 'pj' },
		locked = true,
		distance = 1.5,
		doors = {
			{
				objName = 'v_ilev_gtdoor',
				objCoords = vector3(444.621, -999.001, 30.788)
			},

			{
				objName = 'v_ilev_gtdoor',

				objCoords = vector3(447.218, -999.002, 30.789)
			}
		}
	},
	
	{
		objName = 'slb2k11_SECDOOR',
		objCoords  = vector3(464.159, -1011.259, 33.01), 
		textCoords = vector3(463.44, -1011.30, 32.98), 
		authorizedJobs = { 'police', 'pj', 'offpolice' },
		locked = true,
		distance = 2,
		size = 1
	},
	-- 1
	{
		objName = 'v_ilev_gtdoor',
		objCoords  = vector3(467.194, -996.461, 25.008), 
		textCoords = vector3(467.84, -996.57, 24.91), 
		authorizedJobs = { 'police', 'pj' },
		locked = true,
		distance = 2,
		size = 1
	},
	-- 2
	{
		objName = 'v_ilev_gtdoor',
		objCoords  = vector3(471.477, -996.458, 25.007), 
		textCoords = vector3(472.122, -996.55, 24.91), 
		authorizedJobs = { 'police', 'pj' },
		locked = true,
		distance = 2,
		size = 1
	},
	-- 3
	{
		objName = 'v_ilev_gtdoor',
		objCoords  = vector3(475.755, -996.458, 25.008), 
		textCoords = vector3(476.44, -996.48, 24.91), 
		authorizedJobs = { 'police', 'pj' },
		locked = true,
		distance = 2,
		size = 1
	},
	-- 4
	{
		objName = 'v_ilev_gtdoor',
		objCoords  = vector3(480.031, -996.461, 25.008), 
		textCoords = vector3(480.72, -996.45, 24.91), 
		authorizedJobs = { 'police', 'pj' },
		locked = true,
		distance = 2,
		size = 1
	},
	--
	-- Paleto Bay
	--

	-- Entrance (double doors)
	{
		textCoords = vector3(-443.5, 6016.3, 32.0),
		authorizedJobs = { 'gnr' },
		locked = true,
		distance = 2.5,
		doors = {
			{
				objName = 'v_ilev_shrf2door',
				objYaw = -45.0,
				objCoords  = vector3(-443.1, 6015.6, 31.7),
			},

			{
				objName = 'v_ilev_shrf2door',
				objYaw = 135.0,
				objCoords  = vector3(-443.9, 6016.6, 31.7)
			}
		}
	},

	{
		textCoords = vector3(-448.63, 6007.627, 31.755),   
		authorizedJobs = { 'gnr' },
		locked = true,
		distance = 2,
		doors = {
			{
				objName = 'v_ilev_rc_door2',
				objYaw = 135.0,
				objCoords  = vector3(-447.7092, 6006.717, 31.86633),   
			},

			{
				objName = 'v_ilev_rc_door2',
				objYaw = -45.0,
				objCoords  = vector3(-449.5486, 6008.556, 31.86633) 
			}
		}
	},
	
	--Portas traseiras
	{
		objName = 'v_ilev_rc_door2',
		objYaw = 135.0,
		objCoords  = vector3(-447.08, 6002.33, 33.2), 
		textCoords = vector3(-446.63, 6001.99, 31.75), 
		authorizedJobs = { 'gnr' },
		locked = true,
		distance = 2,
		size = 1
	},


	{
		objName = 'v_ilev_rc_door2',
		objYaw = 135.0,
		objCoords  = vector3(-451.34, 6006.54, 32.44), 
		textCoords = vector3(-451.17, 6006.70, 31.88),   
		authorizedJobs = { 'gnr' }, 
		locked = true,
		distance = 2,
		size = 1
	},
	
	--Jail
	
	{
		objName = 'v_ilev_ph_cellgate',
		objYaw = 45.0,
		objCoords  = vector3(-449.51, 6015.37, 24.73),   
		textCoords = vector3(-449.70, 6015.15, 24.73),     
		authorizedJobs = { 'gnr' }, 
		locked = true,
		distance = 2,
		size = 1
	},
	
	{
		objName = 'v_ilev_ph_cellgate',
		objYaw = -45.0,
		objCoords  = vector3(-453.84, 6013.77, 24.73),   
		textCoords = vector3(-453.84, 6013.77, 24.73),     
		authorizedJobs = { 'gnr' }, 
		locked = true,
		distance = 2,
		size = 1
	},
	{
		objName = 'v_ilev_ph_cellgate',
		objYaw = -45.0,
		objCoords  = vector3(-455.92, 6015.94, 24.73),  
		textCoords = vector3(-455.92, 6015.94, 24.73),      
		authorizedJobs = { 'gnr' }, 
		locked = true,
		distance = 2,
		size = 1
	},
	{
		objName = 'v_ilev_ph_cellgate',
		objYaw = -45.0,
		objCoords  = vector3(-451.68, 6011.52, 24.73),  
		textCoords = vector3(-451.68, 6011.52, 24.73),      
		authorizedJobs = { 'gnr' }, 
		locked = true,
		distance = 2,
		size = 1
	},
	{
		objName = 'v_ilev_ph_cellgate',
		objYaw = -45.0,
		objCoords  = vector3(-449.48, 6009.55, 24.73),  
		textCoords = vector3(-449.48, 6009.55, 24.73),      
		authorizedJobs = { 'gnr' }, 
		locked = true,
		distance = 2,
		size = 1
	},
	
	-- Perguntar aos cornos dos presos e tal, cenas de pergunar 
	{
		objName = 'v_ilev_rc_door2',
		objCoords  = vector3(-435.34, 6009.16, 24.73), 
		textCoords = vector3(-435.34, 6009.16, 24.73),  
		authorizedJobs = { 'gnr' },
		locked = true,
		distance = 2,
		size = 1
	},
	{
		objName = 'v_ilev_rc_door2',
		objCoords  = vector3(-444.24,  6000.70, 24.73 ),   
		textCoords = vector3(-444.24,  6000.70, 24.73 ),  
		authorizedJobs = { 'gnr' },
		locked = true,
		distance = 2,
		size = 1
	},
	--
	-- Bolingbroke Penitentiary
	--

	-- Entrance (Two big gates)
	{
		objName = 'prop_gate_prison_01',
		objCoords  = vector3(1844.9, 2604.8, 44.6),
		textCoords = vector3(1844.9, 2608.5, 48.0),
		authorizedJobs = { 'police', 'pj' },
		locked = true,
		distance = 3,
		size = 1
	},

	{
		objName = 'prop_gate_prison_01',
		objCoords  = vector3(1818.5, 2604.8, 44.6),  
		textCoords = vector3(1818.5, 2608.4, 48.0),
		authorizedJobs = { 'police', 'pj' },
		locked = true,
		distance = 3,
		size = 1
	},
	
	
	
	
	--
	-- Casa dos Super
	--

	{
		objName = 'ex_prop_door_maze2_roof',
		objYaw = 50.0,
		objCoords  = vector3(-127.063, -1630.426, 32.3828),
		textCoords = vector3(-126.500, -1630.426, 32.3828),
		authorizedJobs = {'superdragoes'},
		locked = true,
		distance = 3,
		size = 1
	},
	
	{
		objName = 'ex_prop_door_maze2_roof',
		objYaw = 50.0,
		objCoords  = vector3(-70.59045, -1619.536, 31.71255),
		textCoords = vector3(-70.59045, -1618.536, 31.71255),
		authorizedJobs = {'superdragoes'},
		locked = true,
		distance = 3,
		size = 1
	},
	
	{
		objName = 'ex_prop_door_maze2_roof',
		objYaw = 50.0,
		objCoords  = vector3(-116.6294, -1665.418, 32.71686),
		textCoords = vector3(-116.6294, -1664.418, 32.71686),
		authorizedJobs = {'superdragoes'},
		locked = true,
		distance = 3,
		size = 1
	},
	
	{
		objName = 'ex_prop_door_maze2_roof',
		objYaw = 50.0,
		objCoords  = vector3(-119.7085, -1668.87, 32.71686),
		textCoords = vector3(-119.7085, -1667.87, 32.71686),
		authorizedJobs = {'superdragoes'},
		locked = true,
		distance = 3,
		size = 1
	},
	
	{
		objName = 'ex_prop_door_maze2_roof',
		objYaw = 50.0,
		objCoords  = vector3(-148.9256, -1583.859, 35.02668),
		textCoords = vector3(-148.9256, -1582.859, 35.02668),
		authorizedJobs = {'superdragoes'},
		locked = true,
		distance = 3,
		size = 1
	},
	
	{
		objName = 'ex_prop_door_maze2_roof',
		objYaw = 50.0,
		objCoords  = vector3(-146.6981, -1633.761, 33.33702),
		textCoords = vector3(-146.6981, -1632.761, 33.33702),
		authorizedJobs = {'superdragoes'},
		locked = true,
		distance = 3,
		size = 1
	},
	
	{
		objName = 'ex_prop_door_maze2_roof',
		objYaw = 50.0,
		objCoords  = vector3(-134.0601, -1566.26, 34.5128),
		textCoords = vector3(-134.0601, -1565.26, 34.5128),
		authorizedJobs = {'superdragoes'},
		locked = true,
		distance = 3,
		size = 1
	},
	
	{
		objName = 'ex_prop_door_maze2_roof',
		objYaw = -40.0,
		objCoords  = vector3(-83.22047, -1522.996, 34.48574),
		textCoords = vector3(-83.22047, -1523.996, 34.48574),
		authorizedJobs = {'superdragoes'},
		locked = true,
		distance = 3,
		size = 1
	},
	
	{
		objName = 'ex_prop_door_maze2_roof',
		objYaw = 50.0,
		objCoords  = vector3(-114.6251, -1594.766, 33.05391),
		textCoords = vector3(-114.6251, -1593.766, 33.05391),
		authorizedJobs = {'superdragoes'},
		locked = true,
		distance = 3,
		size = 1
	},
	
	{
		objName = 'ex_prop_door_maze2_roof',
		objYaw = 50.0,
		objCoords  = vector3(-102.9164, -1604.558, 32.09125),
		textCoords = vector3(-102.9164, -1603.558, 32.09125),
		authorizedJobs = {'superdragoes'},
		locked = true,
		distance = 3,
		size = 1
	},
	
	
	{
		objName = 'v_ilev_ph_cellgate',
		objCoords  = vector3(-135.2363, -1599.707, 34.98673),
		textCoords = vector3(-136.2363, -1599.707, 34.98673),
		authorizedJobs = {'superdragoes'},
		locked = true,
		distance = 3,
		size = 1
	},
	
	{
		objName = 'ex_prop_door_arcad_roof_l',
		objCoords  = vector3(-91.26347, -1632.765, 31.65368),
		textCoords = vector3(-91.26347, -1633.765, 31.65368),
		authorizedJobs = {'superdragoes'},
		locked = true,
		distance = 3,
		size = 1
	},
	
	{
		objName = 'apa_prop_apa_cutscene_doora',
		objYaw = -40.0,
		objCoords  = vector3(-95.92204, -1635.625, 32.17473),
		textCoords = vector3(-95.92204, -1636.625, 32.17473),
		authorizedJobs = {'superdragoes'},
		locked = true,
		distance = 3,
		size = 1
	},
	
	{
        objName = 'prop_ss1_14_garage_door',
        objCoords  = vector3(-62.37996, 352.7173, 113.2499),
        textCoords = vector3(-62.37996, 352.7173, 113.2499),
        authorizedJobs = {'superdragoes'},
        locked = true,
        distance = 3,
        size = 1
    },
	
	{
		objName = 'apa_prop_apa_cutscene_doora',
		objYaw = -40.0,
		objCoords  = vector3(-103.597, -1629.075, 33.05647),
		textCoords = vector3(-103.597, -1630.075, 33.05647),
		authorizedJobs = {'superdragoes'},
		locked = true,
		distance = 3,
		size = 1
	},
	{
		objName = 'prop_magenta_door',
		objYaw = -150.0,
		objCoords  = vector3(96.09197, -1284.854, 29.43878),
		textCoords = vector3(96.09197, -1285.854, 29.43878),
		authorizedJobs = {'vanilla'},
		locked = true,
		distance = 3,
		size = 1
	},
	{
		objName = 'v_ilev_roc_door2',
		objYaw = 30.0,
		objCoords  = vector3(99.08321, -1293.701, 29.41868),
		textCoords = vector3(100.08321, -1293.701, 29.41868),
		authorizedJobs = {'vanilla'},
		locked = true,
		distance = 3,
		size = 1
	},
	{
		objName = 'v_ilev_door_orangesolid',
		objYaw = -60.0,
		objCoords  = vector3(113.9822, -1297.43, 29.41868),
		textCoords = vector3(112.9822, -1297.43, 29.41868),
		authorizedJobs = {'vanilla'},
		locked = true,
		distance = 3,
		size = 1
	},
	{
		objName = 'prop_strip_door_01',
		objYaw = 30.0,
		objCoords  = vector3(127.9552, -1298.503, 29.41962),
		textCoords = vector3(127.9552, -1297.503, 29.41962),
		authorizedJobs = {'vanilla'},
		locked = true,
		distance = 3,
		size = 1
	},
	
	
	
	{
		textCoords = vector3(-1388.21, -587.38, 31.00),
		authorizedJobs = { 'bahamas' },
		locked = true,
		distance = 2.5,
		doors = {
			{
				objName = 'v_ilev_ph_gendoor006',
				objYaw = 213.0,
				objCoords = vector3(-1389.212, -588.0406, 30.49132)
			},

			{
				objName = 'v_ilev_ph_gendoor006',
				objYaw = 33.0,
				objCoords = vector3(-1387.026, -586.6138, 30.49563)
			}
		}
	},
	
	{
		textCoords = vector3(-1472.75, -14.23, 54.64),
		authorizedJobs = { 'cartel' },
		locked = true,
		distance = 10,
		doors = {
			{
				objName = 'prop_lrggate_01_l',
				objYaw = 10.0,
				objCoords = vector3(-1475.353, -14.71867, 54.89672) 
			},

			{
				objName = 'prop_lrggate_01_r',
				objYaw = 10.0,
				objCoords = vector3(-1469.96, -13.79652, 54.89387)

			}
		}
	},
	
	{
		textCoords = vector3(-1453.70, -32.26, 54.64),
		authorizedJobs = { 'cartel' },
		locked = true,
		distance = 10,
		doors = {
			{
				objName = 'prop_lrggate_01_r',
				objYaw = 252.0,
				objCoords = vector3(-1454.664, -34.73074, 54.86434)  
			},

			{
				objName = 'prop_lrggate_01_l',
				objYaw = 250.0,
				objCoords = vector3(-1452.903, -29.55032, 54.86719) 

			}
		}
	},
	
	{
		objName = 'prop_fnclink_03gate2',
		objYaw = 150.0,
		objCoords  = vector3(962.4217, -141.7247, 73.63819),
		textCoords = vector3(960.00,  -140.24,  74.49), 
		authorizedJobs = {'motoclub'},
		locked = true,
		distance = 15,
		size = 1
	},
	
	--
	-- Diabos
	--
	{
		objName = 'prop_gate_airport_01',
		objYaw = -110.0,
		objCoords  = vector3(307.1833, -190.6256, 60.38193),
		textCoords = vector3(308.78, -187.033, 61.57), 
		authorizedJobs = {'diabos'},
		locked = true,
		distance = 15,
		size = 1
	},
	
	{
		textCoords = vector3(317.41, -231.74, 53.96),
		authorizedJobs = { 'diabos' },
		locked = true,
		distance = 20,
		doors = {
			{
				objName = 'prop_gate_airport_01',
				objYaw = -20.0,
				objCoords = vector3(323.5893, -233.8971, 52.81477)  
			},

			{
				objName = 'prop_gate_airport_01',
				objYaw = 160.0,
				objCoords = vector3(309.923, -228.8182, 52.82532) 

			}
		}
	},
	{
		objName = 'ex_prop_door_maze2_roof',
		objYaw = -20.0,
		objCoords  = vector3(343.3564, -205.2709, 54.23584),
		textCoords = vector3(344.098, -205.432, 54.22), 
		authorizedJobs = {'diabos'},
		locked = true,
		distance = 2,
		size = 1
	},
	{
		objName = 'ex_prop_door_maze2_roof',
		objYaw = -20.0,
		objCoords  = vector3(325.7002, -198.255, 54.36152),
		textCoords = vector3(326.200, -198.47, 54.22), 
		authorizedJobs = {'diabos'},
		locked = true,
		distance = 2,
		size = 1
	},
	
	
	--
	-- Yakuza
	--
	
	
	{
		objName = 'prop_lrggate_01c_r',
		objYaw = 91.0,
		objCoords  = vector3(-2652.752, 1327.773, 147.6059),
		textCoords = vector3(-2652.36, 1326.32, 147.04), 
		authorizedJobs = {'yakuza'},
		locked = true,
		distance = 2,
		size = 1
	},
	{
		objName = 'apa_p_mp_yacht_door_01',
		objYaw = 91.0,
		objCoords  = vector3(-2667.55, 1326.27, 147.19),
		textCoords = vector3(-2667.59, 1326.30, 147.44), 
		authorizedJobs = {'yakuza'},
		locked = true,
		distance = 2,
		size = 1
	},
	{
		objName = 'xm_prop_iaa_base_door_01',
		objYaw = -180.0,
		objCoords  = vector3(-2666.86, 1330.093, 147.44),
		textCoords = vector3(-2667.13, 1330.13, 147.44), 
		authorizedJobs = {'yakuza'},
		locked = true,
		distance = 2,
		size = 1
	},
	{
		objName = 'v_ilev_fib_door2',
		objYaw = -89.0,
		objCoords  = vector3(-2666.639, 1335.485, 152.2324),
		textCoords = vector3(-2666.60, 1336.27, 152.08), 
		authorizedJobs = {'yakuza'},
		locked = true,
		distance = 2,
		size = 1
	},
	{
		objName = 'apa_prop_ss1_mpint_garage2',
		objCoords  = vector3(-2652.44, 1307.366, 146.679),
		textCoords = vector3(-2652.13, 1307.41, 147.60), 
		authorizedJobs = {'yakuza'},
		locked = true,
		distance = 15,
		size = 1
	},
	
	
	{
		objName = 'hei_v_ilev_bk_gate_pris',
		objCoords  = vector3(256.3116, 220.6579, 106.4296),
		textCoords = vector3(0.0, 0.0, -1000.0), 
		authorizedJobs = {'police', 'pj'},
		locked = true,
		distance = 15,
		size = 1
	},
	
	{
		objName = 'hei_v_ilev_bk_gate2_pris',
		objCoords  = vector3(261.99899291992, 221.50576782227, 106.68346405029),
		textCoords = vector3(0.0, 0.0, -1000.0), 
		authorizedJobs = {'police', 'pj'},
		locked = true,
		distance = 15,
		size = 1
	},
	
	{
		objName = 'v_ilev_bk_safegate',
		objCoords  = vector3(251.8576, 221.0655, 101.8324),
		textCoords = vector3(0.0, 0.0, -1000.0), 
		authorizedJobs = {'police', 'pj'},
		locked = true,
		distance = 15,
		size = 1
	},
	{
		objName = 'v_ilev_bk_safegate',
		objCoords  = vector3(261.3004, 214.5051, 101.8324),
		textCoords = vector3(0.0, 0.0, -1000.0), 
		authorizedJobs = {'police', 'pj'},
		locked = true,
		distance = 15,
		size = 1
	},
	



	{
		textCoords = vector3(-948.85, -1476.01, 7.79),
		authorizedJobs = { 'yakuza' },
		locked = true,
		distance = 3.5,
		doors = {
			{
				objName = 'yakuza_gate_l',
				objCoords  = vector3(-949.29, -1474.69, 6.79),
			},

			{
				objName = 'yakuza_gate_r',
				objCoords  = vector3(-948.36, -1477.43, 6.79)
			}
		}
	},
   --
-- Tunners
--

	{
		objName = 'prop_ch2_05d_g_door',
		objCoords  = vector3(97.31452, -1957.949, 21.21751),
		textCoords = vector3(97.43, -1957.71, 21.74), 
		authorizedJobs = {'tunners'},
		locked = true,
		distance = 10,
		size = 1
	},
	{
		objName = 'prop_fncwood_07gate1',
		objCoords  = vector3(80.78714, -1972.884, 20.8885),
		textCoords = vector3(80.33, -1972.46, 20.90), 
		authorizedJobs = {'tunners'},
		locked = true,
		distance = 3,
		size = 1
	},
	{
		objName = 'p_cut_door_01',
		objCoords  = vector3(81.27128, -1953.784, 20.91102),
		textCoords = vector3(80.78, -1953.27, 20.77),
		authorizedJobs = {'tunners'},
		locked = true,
		distance = 3,
		size = 1
	},
	{
		objName = 'tor_door2',
		objCoords  = vector3(83.69, -1959.21, 20.72),
		textCoords = vector3(84.60, -1959.68, 21.922),
		authorizedJobs = {'tunners'},
		locked = true,
		distance = 3,
		size = 1
	},
	{
		objName = 'v_ilev_ph_cellgate',
		objCoords  = vector3(63.05302, -1959.837, 21.05781),
		textCoords = vector3(62.57, -1960.23, 21.50),
		authorizedJobs = {'tunners'},
		locked = true,
		distance = 3,
		size = 1
	},
	{
		objName = 'tor_door',
		objCoords  = vector3(75.95, -1969.95, 21.124),
		textCoords = vector3(75.80, -1969.80, 21.824),
		authorizedJobs = {'tunners'},
		locked = true,
		distance = 3,
		size = 1
	},

   
	--
	-- Ammunation
	--
	{
		textCoords = vector3(17.32, -1114.92, 29.79),
		authorizedJobs = { 'ammu' },
		locked = true,
		distance = 3.5,
		doors = {
			{
				objName = 'v_ilev_gc_door04',
				objCoords  = vector3(16.12, -1114.60, 29.94),
			},

			{
				objName = 'v_ilev_gc_door03',
				objCoords  = vector3(18.572, -1115.49, 29.94)
			}
		}
	},
	
	--
	-- Addons
	--

	--[[
	-- Entrance Gate (Mission Row mod) https://www.gta5-mods.com/maps/mission-row-pd-ymap-fivem-v1
	{
		objName = 'prop_gate_airport_01',
		objCoords  = vector3(420.1, -1017.3, 28.0),
		textCoords = vector3(420.1, -1021.0, 32.0),
		authorizedJobs = { 'police', 'pj' },
		locked = true,
		distance = 14,
		size = 2
	}
	--]]
}