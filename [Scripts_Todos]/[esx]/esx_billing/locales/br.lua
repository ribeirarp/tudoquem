Locales['br'] = {
  ['invoices'] = 'faturas',
  ['invoices_item'] = '%s€',
  ['received_invoice'] = '~r~Recebeste~s~ uma fatura',
  ['paid_invoice'] = '~g~Pagaste~s~ uma fatura de ~r~%s€~s~',
  ['received_payment'] = '~g~Recebeste~s~ um pagamento de ~r~%s€~s~',
  ['player_not_online'] = 'O jogador não está na cidade',
  ['no_money'] = 'Não tens dinheiro suficiente para pagar a fatura',
  ['target_no_money'] = 'O jogador ~r~não~s~ tem dinheiro suficiente para pagar esta multa!',
}
