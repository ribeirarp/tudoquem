Locales ['fr'] = {
	['put_hifi'] = 'Você acabou de colocar a BoomBox',
    ['get_hifi'] = 'Pegua o BoomBox',
    ['play_music'] = 'Comece a música',
    ['stop_music'] = 'Para a música',
    ['volume_music'] = 'Ajustar o volume',
    ['hifi_alreadyOne'] = 'Você já tem um BoomBox',
    ['set_volume'] = 'Coloque o nível do volume (entre 0 e 100)',
    ['play_id'] = 'Coloque o ID do vídeo do YouTube',
    ['sound_limit'] = 'O volume deve estar entre 0 e 100',
    ['hifi_help'] = 'pressione ~ INPUT_CONTEXT ~ para ajustar a música'
}
