Config                            = {}
Config.DrawDistance               = 100.0
--language currently available EN and SV
Config.Locale                     = 'en'

Config.Zones = {

  MechanicDuty = {
    Pos   = { x = -1159.69, y = -1724.40, z =  4.44-0.98 },    
    Size  = { x = 2.5, y = 2.5, z = 1.5 },
    Color = { r = 0, g = 255, b = 0 },  
    Type  = 27,
  },

 
}