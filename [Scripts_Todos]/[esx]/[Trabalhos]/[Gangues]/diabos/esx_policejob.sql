INSERT INTO `addon_account` (name, label, shared) VALUES
	('society_diabos', 'diabos', 1)
;

INSERT INTO `datastore` (name, label, shared) VALUES
	('society_diabos', 'diabos', 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
	('society_diabos', 'diabos', 1)
;

INSERT INTO `jobs` (name, label) VALUES
	('diabos','LSPD')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
	('diabos',0,'recruit','Recrue',20,'{}','{}'),
	('diabos',1,'officer','Officier',40,'{}','{}'),
	('diabos',2,'sergeant','Sergent',60,'{}','{}'),
	('diabos',3,'lieutenant','Lieutenant',85,'{}','{}'),
	('diabos',4,'boss','Commandant',100,'{}','{}')
;

CREATE TABLE `fine_types` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`label` varchar(255) DEFAULT NULL,
	`amount` int(11) DEFAULT NULL,
	`category` int(11) DEFAULT NULL,

	PRIMARY KEY (`id`)
);

