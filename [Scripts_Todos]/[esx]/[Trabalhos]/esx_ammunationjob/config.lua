Config                            = {}

Config.DrawDistance               = 100.0
Config.MarkerType                 = 22
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.MarkerColor                = { r = 50, g = 50, b = 204 }

Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- enable if you're using esx_identity
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = true -- enable if you're using esx_license

Config.EnableHandcuffTimer        = true -- enable handcuff timer? will unrestrain player after the time ends
Config.HandcuffTimer              = 10 * 60000 -- 10 mins

Config.EnableJobBlip              = false -- enable blips for colleagues, requires esx_society

Config.MaxInService               = -1
Config.Locale                     = 'br'

Config.AmmuStations = {

	Ammunation = {

		Blip = {
			Pos     = { x = 18.55, y = -1112.04, z = 29.79 },  
			Sprite  = 110,
			Display = 4,
			Scale   = 1.2,
			Colour  = 81,
		},

		Armories = {
			{ x = 21.54, y = -1105.17, z = 29.79 },   
		},

		Vehicles = {
			{
				Spawner    = { x = -6.84, y = -1107.93, z = 28.922 },  
				SpawnPoints = {
					{x = -9.37, y = -1114.80, z =  28.225, heading = 164.08, radius = 6.0 }  
				}
			}
		},

		VehicleDeleters = {
			{ x = -1.97, y = -1112.91, z = 28.69-0.98 }   
		}, 

		BossActions = {
			{ x = 23.93, y = -1107.66, z = 29.79 }  
		},

	},

}

Config.AuthorizedWeapons = {
	soldato = {					
	  { weapon = 'WEAPON_FLASHLIGHT',                price = 500 },
	  { weapon = 'WEAPON_BAT',            		     price = 1000 },
	  { weapon = 'WEAPON_CROWBAR',               	 price = 1200 },
      { weapon = 'WEAPON_KNIFE',                     price = 1500},
	  { weapon = 'WEAPON_SWITCHBLADE',               price = 2000 }

	  
	},

	capo = {
      { weapon = 'WEAPON_FLASHLIGHT',                price = 500 },
	  { weapon = 'WEAPON_BAT',            		     price = 1000 },
	  { weapon = 'WEAPON_CROWBAR',               	 price = 1200 },
      { weapon = 'WEAPON_KNIFE',                     price = 1500},
	  { weapon = 'WEAPON_SWITCHBLADE',               price = 2000 }

	},

	consigliere = {
     { weapon = 'WEAPON_FLASHLIGHT',                price = 500 },
	  { weapon = 'WEAPON_BAT',            		     price = 1000 },
	  { weapon = 'WEAPON_CROWBAR',               	 price = 1200 },
      { weapon = 'WEAPON_KNIFE',                     price = 1500},
	  { weapon = 'WEAPON_SWITCHBLADE',               price = 2000 }
	},

	righthand = {
      { weapon = 'WEAPON_FLASHLIGHT',                price = 500 },
	  { weapon = 'WEAPON_BAT',            		     price = 1000 },
	  { weapon = 'WEAPON_CROWBAR',               	 price = 1200 },
      { weapon = 'WEAPON_KNIFE',                     price = 1500},
	  { weapon = 'WEAPON_SWITCHBLADE',               price = 2000 } 
	},

	boss = {
      { weapon = 'WEAPON_FLASHLIGHT',                price = 500 },
	  { weapon = 'WEAPON_BAT',            		     price = 1000 },
	  { weapon = 'WEAPON_CROWBAR',               	 price = 1200 },
      { weapon = 'WEAPON_KNIFE',                     price = 1500},
	  { weapon = 'WEAPON_SWITCHBLADE',               price = 2000 },
	  { weapon = 'gadget_parachute', price = 3000 },
      { weapon = 'WEAPON_PISTOL', components = { 0, 30000, 250, 20000, 5000, nil },                   price = 30000 },
      --{ weapon = 'WEAPON_PISTOL50', components = { 0, 30000, 250, 20000, 5000, nil },                 price = 15000 },
	  --{ weapon = 'WEAPON_APPISTOL', components = { 0, 0, 1000, 4000, nil }, price = 20000 },
      --{ weapon = 'WEAPON_SAWNOFFSHOTGUN', components = { 5000, nil },           price = 30000 },
	  --{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 2000, 6000, nil }, price = 30000 },
	  { weapon = 'WEAPON_MICROSMG', components = { 0, 30000, nil },                  price = 115000 }
	  --{ weapon = 'WEAPON_BULLPUPSHOTGUN', components = { 250, 20000, 25000, nil },            price = 40000 },
	  --{ weapon = 'WEAPON_COMPACTRIFLE', components = { 0, 30000, 50000, nil },               price = 70000 },
	  
	  --{ weapon = 'WEAPON_SMG', components = { 0, 30000, 50000, 250, 15000, 20000, 5000, nil },                       price = 65000 }
	}
}

-- https://wiki.rage.mp/index.php?title=Vehicles
Config.AuthorizedVehicles = {
	Shared = {
		{
			model = 'camry55',
			label = 'Toyota Camry'
		}
	},

	soldato = {
		
	},

	capo = {
		
	},
	consigliere = {
		
    }, 

	righthand = {
		
	},
	
	boss = {
		
	}
}