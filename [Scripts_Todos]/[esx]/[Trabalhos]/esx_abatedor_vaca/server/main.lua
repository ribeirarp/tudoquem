ESX                = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('esx_abatedor_vaca:reward')
AddEventHandler('esx_abatedor_vaca:reward', function(Weight)
    local xPlayer = ESX.GetPlayerFromId(source)

    if Weight >= 1 then
        xPlayer.addInventoryItem('meat_vaca', math.random(1, 4))
    elseif Weight >= 9 then
        xPlayer.addInventoryItem('meat_vaca', math.random(1, 4))
    elseif Weight >= 15 then
        xPlayer.addInventoryItem('meat_vaca', math.random(1, 4))
    end       
end)

RegisterServerEvent('esx_abatedor_vaca:sell')
AddEventHandler('esx_abatedor_vaca:sell', function()
    local xPlayer = ESX.GetPlayerFromId(source)

    local meat_vacaPrice = 125

    local meat_vacaQuantity = xPlayer.getInventoryItem('meat_vaca').count

    if meat_vacaQuantity > 0 then
        xPlayer.addMoney(meat_vacaQuantity * meat_vacaPrice)

        xPlayer.removeInventoryItem('meat_vaca', meat_vacaQuantity)
        TriggerClientEvent('esx:showNotification', xPlayer.source, 'Você vendeu ' .. meat_vacaQuantity .. ' por $' .. meat_vacaPrice * meat_vacaQuantity)
    else
        TriggerClientEvent('esx:showNotification', xPlayer.source, 'Você não tem nada pra vender')
    end
        
end)

function sendNotification(xsource, message, messageType, messageTimeout)
    TriggerClientEvent('notification', xsource, message)
end