ESX                = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('esx_abatedor_porco:reward')
AddEventHandler('esx_abatedor_porco:reward', function(Weight)
    local xPlayer = ESX.GetPlayerFromId(source)

    if Weight >= 1 then
        xPlayer.addInventoryItem('meat_porco', math.random(1, 3))
    elseif Weight >= 9 then
        xPlayer.addInventoryItem('meat_porco', math.random(1, 3))
    elseif Weight >= 15 then
        xPlayer.addInventoryItem('meat_porco', math.random(1, 3))
    end       
end)

RegisterServerEvent('esx_abatedor_porco:sell')
AddEventHandler('esx_abatedor_porco:sell', function()
    local xPlayer = ESX.GetPlayerFromId(source)

    local meat_porcoPrice = 125

    local meat_porcoQuantity = xPlayer.getInventoryItem('meat_porco').count

    if meat_porcoQuantity > 0 then
        xPlayer.addMoney(meat_porcoQuantity * meat_porcoPrice)

        xPlayer.removeInventoryItem('meat_porco', meat_porcoQuantity)
        TriggerClientEvent('esx:showNotification', xPlayer.source, 'Você vendeu ' .. meat_porcoQuantity .. ' por $' .. meat_porcoPrice * meat_porcoQuantity)
    else
        TriggerClientEvent('esx:showNotification', xPlayer.source, 'Você não tem nada pra vender')
    end
        
end)

function sendNotification(xsource, message, messageType, messageTimeout)
    TriggerClientEvent('notification', xsource, message)
end