ESX                = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('esx_abatedor_frango:reward')
AddEventHandler('esx_abatedor_frango:reward', function(Weight)
    local xPlayer = ESX.GetPlayerFromId(source)

    if Weight >= 1 then
        xPlayer.addInventoryItem('meat_frango', math.random(1, 2))
    elseif Weight >= 9 then
        xPlayer.addInventoryItem('meat_frango', math.random(1, 2))
    elseif Weight >= 15 then
        xPlayer.addInventoryItem('meat_frango', math.random(1, 2))
    end       
end)

RegisterServerEvent('esx_abatedor_frango:sell')
AddEventHandler('esx_abatedor_frango:sell', function()
    local xPlayer = ESX.GetPlayerFromId(source)

    local meat_frangoPrice = 125

    local meat_frangoQuantity = xPlayer.getInventoryItem('meat_frango').count

    if meat_frangoQuantity > 0 then
        xPlayer.addMoney(meat_frangoQuantity * meat_frangoPrice)

        xPlayer.removeInventoryItem('meat_frango', meat_frangoQuantity)
        TriggerClientEvent('esx:showNotification', xPlayer.source, 'Você vendeu ' .. meat_frangoQuantity .. ' por $' .. meat_frangoPrice * meat_frangoQuantity)
    else
        TriggerClientEvent('esx:showNotification', xPlayer.source, 'Você não tem nada pra vender')
    end
        
end)

function sendNotification(xsource, message, messageType, messageTimeout)
    TriggerClientEvent('notification', xsource, message)
end