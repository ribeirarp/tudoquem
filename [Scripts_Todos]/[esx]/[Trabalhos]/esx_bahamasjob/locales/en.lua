Locales['en'] = {
    -- Cloakroom
    ['cloakroom']                = 'Vestuário',
    ['citizen_wear']             = 'Roupa casual',
    ['barman_outfit']            = 'Roupa de bartender',
    ['dancer_outfit_1']          = 'Roupa de dança 1',
    ['dancer_outfit_2']          = 'Roupa de dança 2',
    ['dancer_outfit_3']          = 'Roupa de dança 3',
    ['dancer_outfit_4']          = 'Roupa de dança 4',
    ['dancer_outfit_5']          = 'Roupa de dança 5',
    ['dancer_outfit_6']          = 'Roupa de dança 6',
    ['dancer_outfit_7']          = 'Roupa de dança 7',
    ['no_outfit']                = 'There\'s no uniform to fit you...',
    ['open_cloackroom']          = 'Pressionar ~INPUT_CONTEXT~ para mudar',
    -- Vault  
    ['get_weapon']               = 'Pegar arma',
    ['put_weapon']               = 'Depositar arma',
    ['get_weapon_menu']          = 'Cofre - tirar arma',
    ['put_weapon_menu']          = 'Cofre - Depositar arma',
    ['get_object']               = 'Pegar objeto',
    ['put_object']               = 'Depositar objeto',
    ['vault']                    = 'Cofre',
    ['open_vault']               = 'Pressionar ~INPUT_CONTEXT~ abrir o cofre',
  
    -- Fridge  
    ['get_object']               = 'Tirar objeto',
    ['put_object']               = 'Colocar objeto',
    ['fridge']                   = 'Frigorífico',
    ['open_fridge']              = 'Pressiona ~INPUT_CONTEXT~ para aceder ao frigorífico',
    ['bahamas_fridge_stock']     = 'Frigorífico do Bahamas',
    ['fridge_inventory']         = 'Conteúdo do Frigorífico',
  
    -- Shops  
    ['shop']                     = 'Loja do Bahamas',
    ['shop_menu']                = 'Pressionar ~INPUT_CONTEXT~ para acessar',
    ['bought']                   = 'Compraste ~b~ 1x',
    ['not_enough_money']         = 'Não tens dinheiro suficiente.',
    ['max_item']                 = 'Já tens o suficiente contigo.',
  
    -- Vehicles  
    ['vehicle_menu']             = 'Veiculo',
    ['vehicle_out']              = 'Já existe um veiculo fora da garagem',
    ['vehicle_spawner']          = 'Pressionar ~INPUT_CONTEXT~ para retirar veiculo',
    ['store_vehicle']            = 'Pressionar ~INPUT_CONTEXT~ para guardar o veiculo',
    ['service_max']              = 'Serviço maximo: ',
    ['spawn_point_busy']         = 'Um veículo está perto do ponto de saída',
  
    -- Boss Menu  
    ['take_company_money']       = 'Remover empresa de dinheiro',
    ['deposit_money']            = 'Depositar dinheiro',
    ['amount_of_withdrawal']     = 'Quantidade de retirada',
    ['invalid_amount']           = 'Montante inválido',
    ['amount_of_deposit']        = 'Valor do depósito',
    ['open_bossmenu']            = 'Pressionar ~INPUT_CONTEXT~ para abrir o menu',
    ['invalid_quantity']         = 'Quantidade inválida',
    ['you_removed']              = 'Tu removeste x',
    ['you_added']                = 'Tu adicionaste x',
    ['quantity']                 = 'Montante',
    ['inventory']                = 'Inventário',
    ['bahamas_stock']            = 'Stock do bahamas',
  
    -- Billing Menu  
    ['billing']                  = 'Fatura',
    ['no_players_nearby']        = 'Nao tens jogadores perto de ti',
    ['billing_amount']           = 'Montante da Fatura',
    ['amount_invalid']           = 'Montante Invalida',
  
    -- Crafting Menu  
    ['crafting']                 = 'Craft',
    ['martini']                  = 'White Martini',
    ['icetea']                   = 'Ice Tea',
    ['drpepper']                 = 'Dr. Pepper',
    ['saucisson']                = 'Sausage',
    ['grapperaisin']             = 'Bunch of grapes',
    ['energy']                   = 'Energy Drink',
    ['jager']                    = 'Jägermeister',
    ['limonade']                 = 'Lemonade',
    ['vodka']                    = 'Vodka',
    ['ice']                      = 'Ice',
    ['soda']                     = 'Soda',
    ['whisky']                   = 'Whisky',
    ['rhum']                     = 'Rhum',
    ['tequila']                  = 'Tequila',
    ['menthe']                   = 'Mint',
    ['jusfruit']                 = 'Fruit juice',
    ['jagerbomb']                = 'Jägerbomb',
    ['bolcacahuetes']            = 'Peanuts',
    ['bolnoixcajou']             = 'Cashew nuts',
    ['bolpistache']              = 'Pistachios',
    ['bolchips']                 = 'Crisps',
    ['jagerbomb']                = 'Jägerbomb',
    ['golem']                    = 'Golem',
    ['whiskycoca']               = 'Whisky-coke',
    ['vodkaenergy']              = 'Vodka-energy',
    ['vodkafruit']               = 'Vodka-fruit juice',
    ['rhumfruit']                = 'Rum-fruit juice',
    ['teqpaf']                   = 'Teq\'paf',
    ['rhumcoca']                 = 'Rhum-coke',
    ['mojito']                   = 'Mojito',
    ['mixapero']                 = 'Aperitif Mix',
    ['metreshooter']             = 'Shooter meter',
    ['jagercerbere']             = 'Jäger Cerberus',
    ['assembling_cocktail']      = 'Mistura de ingredientes diferentes em andamento!',
    ['craft_miss']               = 'Fracasso infeliz da mistura ...',
    ['not_enough']               = 'Insuficiente ~r~ ',
    ['craft']                    = 'Mistura completa de ~g~',
  
    -- Misc  
    ['map_blip']                 = 'bahamas ',
    ['bahamas']                  = 'bahamas ',
  
    -- Phone  
    ['bahamas_phone']            = 'bahamas',
    ['bahamas_customer']         = 'Citizen',
  
    -- Teleporters
    ['e_to_enter_1']             = 'Pressiona ~INPUT_PICKUP~ para ir para trás do bar',
    ['e_to_exit_1']              = 'Pressiona ~INPUT_PICKUP~ para a frente do bar',
    ['e_to_enter_2']             = 'Pressiona ~INPUT_PICKUP~ para cima do telhado.',
    ['e_to_exit_2']              = 'Pressiona ~INPUT_PICKUP~ para ir para o escritório.',
    
  -- Action Menu
  ['citizen_interaction'] = 'Interagir com o cidadão',
  
  ['id_card'] = 'Carteira de identidade',
  ['search'] = 'Revistar',
  ['handcuff'] = 'Algemar / Soltar',
  ['drag'] = 'Pegar',
  ['put_in_vehicle'] = 'Colocar no veículo',
  ['out_the_vehicle'] = 'Tirar do veiculo',
  ['fine'] = 'Multa',
  ['no_players_nearby'] = 'Nenhum jogador nas proximidades',  

  -- ID Card Menu
  ['name'] = 'Nome : ',
  ['bac'] = 'bAC : ',
  
  -- Body Search Menu
  ['confiscate_dirty'] = 'Confiscar dinheiro sujo : $',
  ['guns_label'] = '--- Armas ---',
  ['confiscate'] = 'Confiscar ',
  ['inventory_label'] = '--- Inventário ---',
  ['confiscate_inv'] = 'Confiscar x',  
  
  -- Notifications
  ['from'] = '~s~ de ~b~',
  ['you_have_confinv'] = 'Você confiscou ~y~x',
  ['confinv'] = '~s~ confiscado de você ~y~x',
  ['you_have_confdm'] = 'Você confiscou ~y~$',
  ['confdm'] = '~s~ confiscado de você ~y~$',
  ['you_have_confweapon'] = 'Você confiscou ~y~x1 ',
  ['confweapon'] = '~s~ confiscado de você ~y~x1 ',
  
  }