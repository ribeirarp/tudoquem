-----------------------------------------
-- Created and modify by L'ile Légale RP
-- SenSi and Kaminosekai
-----------------------------------------
Locales['en'] = {
  -- General
  ['cloakroom'] = 'Vestiário',
  ['vine_clothes_civil'] = 'Roupa de civil',
  ['vine_clothes_vine'] = 'Roupa de trabalho',
  ['veh_menu'] = 'Garagem',
  ['not_enough_place'] = 'you don\' have enought place',
  ['sale_in_prog'] = 'Venda em progresso...',
  ['van'] = 'work Truck',
  ['open_menu'] = ' ',
  ['spawn_veh'] = 'Pressiona ~INPUT_CONTEXT~ para tirar o veículo da garagem.',
  ['amount_invalid'] = 'Quantia inválida',
  ['press_to_open'] = 'Pressiona ~INPUT_CONTEXT~ para aceder ao menu',
  ['billing'] = 'Faturação',
  ['invoice_amount'] = 'Valor da fatura',
  ['no_players_near'] = 'Sem players por perto',
  ['store_veh'] = 'Pressiona ~INPUT_CONTEXT~ para guardar o veículo',
  ['comp_earned'] = 'A tua empresa ganhou ~g~€',
  ['deposit_stock'] = 'Depositar no stock',
  ['take_stock'] = 'Retirar do stock',
  ['boss_actions'] = 'Menu administrativo',
  ['quantity'] = 'Quantia',
  ['quantity_invalid'] = 'Quantia inválida',
  ['inventory'] = 'Inventário',
  ['have_withdrawn'] = 'Removeste x',
  ['added'] = 'Adicionaste x',
  
  -- Depend to the company
  ['vigneron_client'] = 'Cliente',
  ['transforming_in_progress'] = 'Transformação das uvas em progresso... ',
  ['raisin_taken'] = 'Estás a apanhar uvas.',
  ['press_traitement'] = 'Pressiona ~INPUT_CONTEXT~ para transformar as uvas.',
  ['press_collect'] = 'Pressiona ~INPUT_CONTEXT~ para recolher uvas.',
  ['press_sell'] = 'Pressiona ~INPUT_CONTEXT~ para vender os produtos.',
  ['no_jus_sale'] = 'Não tens sumo de uva suficiente.',
  ['no_vin_sale'] = 'Não tens vinho suficiente.',
  ['not_enough_raisin'] = 'Não tens uvas suficientes.',
  ['grand_cru'] = '~g~Parabéns! Conseguiste uma garrafa Glen Moray !~w~',
  ['no_product_sale'] = 'Não tens produtos suficientes.',
  ['press_traitement_jus'] = 'Pressiona ~INPUT_CONTEXT~ para transformar as uvas.',
  ['used_jus'] = 'Bebeste um sumo de uva.',
  ['used_grand_cru'] = 'Bebeste uma garrafa de vinho do Glen Moray.',
  ['used_vine'] = 'Bebeste uma garrafa de vinho.',
}