Locales ['en'] = {
	['used_kit']					= 'Tu usas-te x1 lockpick',
	['must_be_outside']				= 'Você deve estar fora do veículo!',
	['no_vehicle_nearby']			= 'Não há veículo nas proximidades',
	['vehicle_unlocked']			= 'Você clicou no veículo!',
	['abort_hint']					= 'Pressiona ~INPUT_VEH_DUCK~ para cancelar',
	['aborted_lockpicking']		    = 'Tu Cancelas-te a entrada a casa',
}
