Locales ['en'] = {
  -- policia
  ['bca_pass'] = 'Teste negativo com menos de 0,01%',
  ['drug_pass'] = 'Teste negativo para uso de estupefacientes',
  ['drug_fail'] = 'Teste positivo para uso de estupefacientes',
  ['fail_drunk'] = 'Teste positivo com 0,16%',
  ['fail_tipsy'] = 'Teste positivo com 0.06%',
  ['restriced_zone'] = 'Alguem entrou numa area restrita, responda imediatamente!',
  ['police_alert'] = 'Algum otario esta a vender drogas',
  ['act_imp_police'] = 'O comprador gosta de dar oportunidade  a policia de o apanhar, espera ate que algum policia esteja de servico',
  
  -- weed
  ['weed_pickupprompt'] = 'Pressiona ~INPUT_CONTEXT~ para colher ~g~Erva~s~.',
  ['weed_field_close'] = 'Tambem estas a sentir este cheiro a Erva?',
  ['weed_process_close'] = 'Com um cheiro destes parece que ha por aqui uma plantacao',
  ['weed_inventoryfull'] = 'Nao tens mais espaco nos bolsos para ~g~Erva~s~.',
  ['weed_processprompt'] = 'Pressiona ~INPUT_CONTEXT~ para iniciar o ~g~processo de Erva~s~s~.',
  ['weed_processingstarted'] = 'Processando  a ~g~Erva~s~ em ~g~Marijuana~s~...',
  ['weed_processingfull'] = 'Processamento~r~cancelado~s~ tens os bolsos cheios!',
  ['weed_processingenough'] = 'Tu deves ter ~b~1x~s~ ~g~Erva~s~ para processares',
  ['weed_processed'] = 'Processas-te ~b~1x~s~ ~g~Erva~s~ em ~b~5x~s~ ~g~Cabecos de erva~s~',
  ['weed_processingtoofar'] = 'O processamento foi ~r~cancelado~s~ porque deste ghost.',
  ['weed_sell'] = 'Pressiona ~INPUT_CONTEXT~  para entregares a tua erva',
  ['weed_use'] = 'Fumas-te um charro',

  -- cocaine
  ['cocaine_pickupprompt'] = 'Pressiona ~INPUT_CONTEXT~ para colher a planta de cocaina.',
  ['cocaine_field_close'] = 'Estas plantas nao parecem ser algodao',
  ['cocaine_process_close'] = 'Estes gajos estao a  fabricar a propria cocaina',
  ['cocaine_inventoryfull'] = 'Nao tens mais espaco nos bolsos para ~g~Plantas de cocaina~s~.',
  ['cocaine_processprompt'] = 'Pressiona ~INPUT_CONTEXT~ para iniciar o ~g~processo da planta de cocaina~s~.',
  ['cocaine_processingstarted'] = 'Processando a ~g~planta de cocaina~s~ em ~g~cocaina~s~ ...',
  ['cocaine_processingfull'] = 'Processamento ~r~cancelado~s~ tens os bolsos cheios!',
  ['cocaine_processingenough'] = 'Tu deves ter ~b~3x~s~ ~g~plantas de cocaina~ para processares',
  ['cocaine_processed'] = 'Tu processas-te~b~3x~s~ ~g~plantas de cocaina~s~ para ~b~1x~s~ ~g~cocaina~s~',
  ['cocaine_processingtoofar'] = 'O processamento foi ~r~cancelado~s~ porque deste ghost.',
  ['cocaine_sell'] = 'Pressiona ~INPUT_CONTEXT~ para entregar a coca a The Golfing Society',
  ['cocaine_use'] = 'Cheiraste muita coca!',

  -- ephedrine
  ['ephedrine_pickupprompt'] = 'Pressiona~INPUT_CONTEXT~ para colher a ~g~planta de efedra.',
  ['ephedrine_field_close'] = '~r~ Estas numa area restrita, baza, alguem chamou a bofia! ',
  ['ephedrine_process_close'] = 'Tambem estas a sentir este cheiro a  podre?',
  ['ephedrine_inventoryfull'] = 'Nao tens mais espaco nos bolsos para ~g~Efedra~s~.',
  ['ephedrine_processprompt'] = 'Pressiona ~INPUT_CONTEXT~ para iniciar o ~g~Processo de Efedra~s~.',
  ['ephedrine_processingstarted'] = 'Processando ~g~Efedra~s~ em ~g~Ephedrine~s~ ...',
  ['ephedrine_processingfull'] = 'Processamento ~r~cancelado~s~ tens os bolsos cheios!',
  ['ephedrine_processingenough'] = 'Tu deves ter ~b~1x~s~ ~g~efedra~s~ para processares.',
  ['ephedrine_processed'] = 'Processas-te ~b~1x~s~ ~g~efedra~s~ para ~b~1x~s~ ~g~efedrina~s~',
  ['ephedrine_processingtoofar'] = 'O processamento foi cancelado porque deste ghost.',
  ['ephedrine_sell'] = 'Pressiona ~INPUT_CONTEXT~',

  -- meth
  -- ['meth_pickupprompt'] = 'Pressiona ~INPUT_CONTEXT~ para colher a planta de Efedrina.',
  -- ['meth_inventoryfull'] = 'Nao tens mais espaco nos bolsos para ~g~Ephedrine~s~.',
  ['meth_process_close'] = 'Tambem estas a sentir este cheiro a  mijo?',
  ['meth_processprompt'] = 'Pressiona ~INPUT_CONTEXT~ para iniciar o ~g~Processo de efedrina~s~.',
  ['meth_processingstarted'] = 'Processando ~g~Efedrina~s~ em ~g~Metanfetaminas~s~ ',
  ['meth_processingfull'] = 'Processamento ~r~cancelado~s~ tens os bolsos cheios!',
  ['meth_processingenough'] = 'Tu deves ter ~b~2x~s~ ~g~efedrina~s~ para processares.',
  ['meth_processed'] = 'Processas-te ~b~2x~s~ ~g~efedrina~s~ para ~b~1x~s~ ~g~Metanfetaminas~s~',
  ['meth_processingtoofar'] = 'O processamento foi cancelado porque deste ghost.',
  ['meth_sell'] = 'Pressiona ~INPUT_CONTEXT~ para entregar a metanfetamina para Willie',
  ['meth_use'] = 'Ui, quando isto bater estas tramado',

  -- crack
  -- ['crack_pickupprompt'] = 'Pressiona ~INPUT_CONTEXT~ para colher a planta de g efedrina.',
  -- ['crack_inventoryfull'] = 'Nao tens mais espaco nos bolsos para ~g~Ephedrine~s~.',
  ['crack_process_close'] = 'The Lost nao e muito amigavel quando se trata de partilhar drogas',
  ['crack_processprompt'] = 'Pressiona ~INPUT_CONTEXT~ para iniciar  o ~g~Processo de Cocaina~s~.',
  ['crack_processingstarted'] = 'Processando ~g~cocaina~ em ~g~crack~s~ ...',
  ['crack_processingfull'] = 'Processamento ~r~cancelado~s~ tens os bolsos cheios!',
  ['crack_processingenough'] = 'Tu deves ter ~b~2x~s~ ~g~Cocaina~s~ para processares.',
  ['crack_processed'] = 'Processas-te ~b~2x~s~ ~g~cocaina~s~ para ~b~1x~s~ ~g~crack~s~',
  ['crack_processingtoofar'] = 'O processamento foi cancelado porque deste ghost.',
  ['crack_sell'] = 'Pressiona ~INPUT_CONTEXT~ para entregar a Jerome',
  ['crack_use'] = 'Provavelmente nao deixarias cair o crack se nao o tivesses fumado',

  -- opio
  ['opium_pickupprompt'] = 'Pressiona ~INPUT_CONTEXT~ para colher a planta ~g~Papoula~s~.',
  ['opium_field_close'] = '~ r ~ Se a familia Wong te apanha, arrancam-te a cabeça por andares nos seus campos de Papoulas',
  ['opium_process_close'] = 'Estas Papoulas sao no minimo suspeitas',
  ['opium_inventoryfull'] = 'Nao tens mais espaco nos bolsos para ~g~Papoulas~s~.',
  ['opium_processprompt'] = 'Pressiona ~INPUT_CONTEXT~ para iniciar o ~g~processo das Papoulas~s~.',
  ['opium_processingstarted'] = 'Processando ~g~papoula~s~ em ~g~Opio~s~ ...',
  ['opium_processingfull'] = 'Processamento ~r~cancelado~s~ tens os bolsos cheios!' ,
  ['opium_processingenough'] = 'Tu deves ter ~b~2x~s~ ~g~Papoulas~s~ para processares',
  ['opium_processed'] = 'Processas-te ~b~2x~s~ ~g~Papoulas~s~ para ~b~1x~s~ ~g~ Opio~s',
  ['opium_processingtoofar'] = 'O processamento foi cancelado porque deste ghost.',
  ['opium_sell'] = 'Pressiona ~INPUT_CONTEXT~ para jogares o Opio na lareira dos ONeil, eles irao pagar nao te preocupes',

   -- heroina
  -- ['heroine_pickupprompt'] = 'Pressiona ~INPUT_CONTEXT~ para colher a planta de Efedrina.',
  -- ['heroine_inventoryfull'] = 'Nao tens mais espaco nos bolsos para ~g~Efedrina~s~.',
  ['heroine_process_close'] = 'Pode ser que o Trevor tenha algum trabalho para ti',
  ['heroine_processprompt'] = 'Pressiona ~INPUT_CONTEXT~ para iniciar  o~g~processo de Opio~s~.',
  ['heroine_processingstarted'] = 'Processando ~g~Opio~s~ em ~g~Heroina~s~ ...',
  ['heroine_processingfull'] = 'Processamento ~r~cancelado~s~tens os bolsos cheios!' ,
  ['heroine_processingenough'] = 'Tu deves ter ~b~5x~s~ ~g~Opio~s~ para processares.',
  ['heroine_processed'] = 'Processas-te ~b~5x~s~ ~g~Opio~s~ para ~b~1x~s~ ~g~Heroina~s',
  ['heroine_processingtoofar'] = 'O processamento foi cancelado porque deste ghost.',
  ['heroine_sell'] = 'Pressiona ~INPUT_CONTEXT~ para entregar heroina ao Frederick',
  ['heroine_use'] = 'Deste na veia, carocho',

  -- traficante de drogas
  ['dealer_prompt'] = 'Pressiona ~INPUT_CONTEXT~ para conversares com o ~r~vendedor~s~.',
  ['dealer_title'] = 'Traficante de drogas',
  ['dealer_item'] = '€%s',
  ['dealer_notenough'] = 'Nao tens o suficiente para vender!',
  ['dealer_sold'] = '~Vendes-te ~b~%sx~s~ ~y~%s~s~ por ~g~%s~s~',

  -- blips
  ['blip_weedprocess'] = 'Hangout hippie',
  ['blip_cokeprocess'] = 'Iate',
  ['blip_ephedrineprocess'] = 'Garagem Vagos',
  ['blip_poppyprocess'] = 'Laboratorios humanitarios',
  ['blip_methprocess'] = 'As de licor',
  ['blip_crackprocess'] = 'Lost MC Clubhouse',
  ['blip_heroineprocess'] = 'Trevors',
  ['blip_weeddump'] = 'Smoke on the Water',
  ['blip_cokedump'] = 'Golf Club',
  ['blip_methdump'] = 'Farmacia de Willie',
  ['blip_heroinedump'] = 'Fredericks Crane ',
  ['blip_opiumdump'] = 'Fazenda ONeil ',
  ['blip_crackdump'] = 'Albergue sem teto',
  ['blip_weedfield'] = 'Fazenda Senora',
  ['blip_cokefield'] = 'Campo de algodao',
  ['blip_poppyfield'] = 'Propriedade Wong',
  ['blip_ephedrinefield'] = 'Area restrita',

  -- misc
  ['not_needed'] = 'Nao deves tomar drogas que nao precisas!',
  ['fake_clean'] = 'Vais ficar limpo num minuto!',
  ['wearin_off'] = 'Granda moca oh Mano!',
  ['comin_down'] = 'Sentes-te a cair do espaço!',
  ['forcado'] = 'Foste forcado a soprar no bafometro',
  ['whisky'] = '>Bebeste o Whisky de penalti',
  ['tequila'] = 'Estas a tentar perder as cuecas esta noite?',
  ['vodka'] = 'Bebes-te a Vodka toda, calma homem',
  ['beer'] = 'Bebes-te uma loira Sagres',
  ['fake_pee'] = 'Bebes-te uma pilula de xixi falsa',
  ['drug_test'] = 'Fizes-te um teste de drogas',
}