local spawnedCoca = 1
local cocaPlants = {}
local isPickingUp, isProcessing = false, false

Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(10)
		local coords = GetEntityCoords(PlayerPedId())

		if GetDistanceBetweenCoords(coords, Config.FieldZones.CocaineField.coords, true) < 30 then
			TriggerEvent('esx:showNotification', _U('cocaine_field_close'))
			SpawnCocaPlants()
			Citizen.Wait(500)
		else
			Citizen.Wait(500)
		end
	end
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		local playerPed = PlayerPedId()
		local coords = GetEntityCoords(playerPed)

		if GetDistanceBetweenCoords(coords, Config.ProcessZones.CocaineProcessing.coords, true) < 15 and GetDistanceBetweenCoords(coords, Config.ProcessZones.CocaineProcessing.coords, true) > 10 then
			ESX.ShowNotification(_U('cocaine_process_close'))
		end
		if GetDistanceBetweenCoords(coords, Config.ProcessZones.CocaineProcessing.coords, true) < 1.5 and not isProcessing then
			ProcessCoca()
			Citizen.Wait(500)
		end
	end
end)

function ProcessCoca()
	isProcessing = true
	ESX.TriggerServerCallback('esx_jk_drugs:verPolicia', function(CopsConnected)
            if CopsConnected >= Config.RequiredCopsCoke then
               TriggerServerEvent('esx_jk_drugs:processCocaPlant')
            	local timeLeft = Config.Delays.CocaineProcessing / 1000
            	local playerPed = PlayerPedId()
            
            	while timeLeft > 0 do
            		Citizen.Wait(1000)
            		timeLeft = timeLeft - 1
            
            		if GetDistanceBetweenCoords(GetEntityCoords(playerPed), Config.ProcessZones.CocaineProcessing.coords, false) > 4 then
            			TriggerServerEvent('esx_jk_drugs:cancelProcessing')
            			break
            		end
            	end
            	isProcessing = false	      	    
 						
            else
            	TriggerEvent('esx:showNotification', 'É preciso ' .. Config.RequiredCopsCoke .. ' polícias na cidade.')
            end
    end)	
end

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		local playerPed = PlayerPedId()
		local coords = GetEntityCoords(playerPed)
		local nearbyObject, nearbyID

		for i=1, #cocaPlants, 1 do
			if GetDistanceBetweenCoords(coords, GetEntityCoords(cocaPlants[i]), false) < 1 then
				nearbyObject, nearbyID = cocaPlants[i], i
			end
		end

		if nearbyObject and IsPedOnFoot(playerPed) and not IsPedUsingAnyScenario(playerPed) then

			if not isPickingUp then
				ESX.ShowHelpNotification(_U('cocaine_pickupprompt'))
			end

			if IsControlJustReleased(0, 38) and not isPickingUp then
				isPickingUp = true
				table.remove(cocaPlants, nearbyID)

				ESX.TriggerServerCallback('esx_jk_drugs:verPolicia', function(CopsConnected)
                     if CopsConnected >= Config.RequiredCopsCoke then
           	      	    ESX.TriggerServerCallback('esx_jk_drugs:canPickUp', function(canPickUp)
        					if canPickUp then
        						TaskStartScenarioInPlace(playerPed, 'world_human_gardener_plant', 0, false)
        
        						
        						spawnedCoca = spawnedCoca - 1
        
        						Citizen.Wait(1250)
        						ClearPedTasks(playerPed)
        						Citizen.Wait(1500)					
        						ESX.Game.DeleteObject(nearbyObject)
        
        						TriggerServerEvent('esx_jk_drugs:pickedUpCocaPlant')
        					else
        						ESX.ShowNotification(_U('cocaine_inventoryfull'))
        					end
    				    end, 'cocaine')
    			    	isPickingUp = false
						
                     else
        	            TriggerEvent('esx:showNotification', 'É preciso ' .. Config.RequiredCopsCoke .. ' polícias na cidade.')
                     end
                end)
			end
		else
			Citizen.Wait(500)
		end
	end
end)

AddEventHandler('onResourceStop', function(resource)
	if resource == GetCurrentResourceName() then
		for k, v in pairs(cocaPlants) do
			ESX.Game.DeleteObject(v)
		end
	end
end)

function SpawnCocaPlants()
	while spawnedCoca < 10 do
		Citizen.Wait(0)
		local cocaCoords = GenerateCocaCoords()

		ESX.Game.SpawnLocalObject('prop_weed_02', cocaCoords, function(obj)
			PlaceObjectOnGroundProperly(obj)
			FreezeEntityPosition(obj, true)

			table.insert(cocaPlants, obj)
			spawnedCoca = spawnedCoca + 1
		end)
	end
end

function ValidateCocaCoord(plantCoord)
	if spawnedCoca > 0 then
		local validate = true

		for k, v in pairs(cocaPlants) do
			if GetDistanceBetweenCoords(plantCoord, GetEntityCoords(v), true) < 5 then
				validate = false
			end
		end

		if GetDistanceBetweenCoords(plantCoord, Config.FieldZones.CocaineField.coords, false) > 50 then
			validate = false
		end

		return validate
	else
		return true
	end
end

function GenerateCocaCoords()
	while true do
		Citizen.Wait(1)

		local cocaCoordX, cocaCoordY

		math.randomseed(GetGameTimer())
		local modX = math.random(-90, 90)

		Citizen.Wait(100)

		math.randomseed(GetGameTimer())
		local modY = math.random(-90, 90)

		cocaCoordX = Config.FieldZones.CocaineField.coords.x + modX
		cocaCoordY = Config.FieldZones.CocaineField.coords.y + modY

		local coordZ = GetCoordZ(cocaCoordX, cocaCoordY)
		local coord = vector3(cocaCoordX, cocaCoordY, coordZ)

		if ValidateCocaCoord(coord) then
			return coord
		end
	end
end

function GetCoordZ(x, y)
	local groundCheckHeights = { 40.0, 41.0, 42.0, 43.0, 44.0, 45.0, 46.0, 47.0, 48.0, 49.0, 50.0 }

	for i, height in ipairs(groundCheckHeights) do
		local foundGround, z = GetGroundZFor_3dCoord(x, y, height)

		if foundGround then
			return z
		end
	end
	return 43.0
end



function ProcessCoke()
	isProcessing = true

	TriggerServerEvent('esx_jk_drugs:processCoke')
	local timeLeft = Config.Delays.CrackProcessing / 1000
	local playerPed = PlayerPedId()

	while timeLeft > 0 do
		Citizen.Wait(1000)
		timeLeft = timeLeft - 1

		if GetDistanceBetweenCoords(GetEntityCoords(playerPed), Config.ProcessZones.CrackProcessing.coords, false) > 4 then
			TriggerServerEvent('esx_jk_drugs:cancelProcessing')
			break
		end
	end
	isProcessing = false
end