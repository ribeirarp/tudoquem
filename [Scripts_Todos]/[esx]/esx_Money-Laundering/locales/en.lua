Locales['en'] = {
	['cloakroom']				= 'Vestuário',
	['job_wear']				= 'Começar a fazer entregas]',
	['citizen_wear']			= 'Roupa de cívil',
	['vehiclespawner']			= 'Escolha de veículo', 
	['already_have_truck']		= 'A empresa já te deu um veículo!', 
	['delivery']				= 'Pressiona ~INPUT_PICKUP~ para entregar.',
	['not_your_truck']			= 'Este não é o veículo que te entregamos!',
	['not_your_truck2']			= 'Este não é o veículo que te entregamos!',
	['need_it']					= 'sim, mas nós precisamos!',
	['ok_work']					= 'ok, para o trabalho agora!',
	['scared_me']				= 'ok, assustaste-me!',
	['resume_delivery']			= 'ok, remote a sua entrega então!',
	['no_delivery']				= 'Sem entrega, sem dinheiro!',
	['pay_repair']				= 'Por outro lado, tu pagas os reparos!',
	['repair_minus']			= 'Reparação do veículo : -',
	['shipments_plus']			= 'Entregas : +',
	['truck_state']				= 'Sem dinheiro! Viste como deixaste o teu veículo?',
	['no_delivery_no_truck']	= 'Sem entrega, sem camião! Isto é uma piada?',
	['truck_price']				= 'Preço do veículo : -',
	['meet_ls']					= 'Vai ao ponto de entrega em Los Santos',
	['meet_bc']					= 'Vai ao ponto de entrega em Blaine County',
	['meet_del']				= 'Vai ao ponto de entrega',
	['return_depot']			= 'Volta à empresa.',
	['blip_job']				= 'Post Office',
	['blip_delivery']			= 'Post Office : Delivery',
	['blip_goal']				= 'Post Office : Delivery point',
	['blip_depot']				= 'Post Office : depot',
	['cancel_mission']			= 'Pressiona ~INPUT_PICKUP~	para ~r~devolver~w~ o veículo',
	['not_delivery']			= '~r~Não fizeste nenhuma entrega.',
	['Money_Laundering']		= 'Lavagem de dinheiro',
	['return_finished']			= 'Pressiona ~INPUT_PICKUP~ para sair de serviço e colectar o ~g~dinheiro limpo'
}

--------------------------------------------------------------------------
------------------------------BY  K R I Z F R O S T-----------------------
--------------------------------------------------------------------------
----- MONEY LAUNDERING SCRIPT RELEASED CREDITS TO ORIGINAL CREATOR--------
----- OF ESX_POSTALJOB RE WORKED AND MODIFIED BY KRIZFROST ---------------
--------------------------------------------------------------------------