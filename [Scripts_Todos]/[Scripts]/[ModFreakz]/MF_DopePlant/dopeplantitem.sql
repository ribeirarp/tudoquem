CREATE TABLE IF NOT EXISTS `dopeplants` (
  `owner` varchar(50) NOT NULL,
  `plant` longtext NOT NULL,
  `plantid` bigint(20) NOT NULL
);


INSERT INTO `items` (`name`,`label`,`limit`) VALUES
	('highgradefemaleseed', 'Semente Femea+', -1),
	('lowgradefemaleseed', 'Semente Femea', -1),
	('highgrademaleseed', 'Semente Macho+', -1),
	('lowgrademaleseed', 'Semente Macho', -1),
	('highgradefert', 'Fertelizante de alta qualidade', -1),
	('lowgradefert', 'Fertelizante de baixa qualidade', -1),
	('purifiedwater', 'Agua purificada', -1),
	('wateringcan', 'Regador', -1),
	('plantpot', 'Vaso', -1),
	('trimmedweed', 'Cabeço de erva', -1),
	('dopebag', 'Saco de plastico', -1),
	('bagofdope', 'Saco de droga', -1),
	('drugscales', 'Balança', -1),



