Config = {}

-- Ammo given by default to crafted weapons
Config.WeaponAmmo = 60

Config.Recipes = {
	-- Can be a normal ESX item
	["lockpick"] = { 
		{item = "iron", quantity = 8 }, 
		{item = "blowtorch", quantity = 1 },
	},
	["extended_magazine"] = { 
		{item = "iron", quantity = 5 }, 
		{item = "blowpipe", quantity = 1 },
	},
	["yusuf"] = { 
		{item = "iron", quantity = 3 }, 
		{item = "blowpipe", quantity = 1 },
	},
	["grip"] = { 
		{item = "iron", quantity = 2 }, 
		{item = "blowpipe", quantity = 1 },
	},
	["blowpipe"] = { 
		{item = "iron", quantity = 5 }, 
		{item = "lighter", quantity = 4 },
	},
	["lazer_scope"] = { 
		{item = "iron", quantity = 4 }, 
		{item = "blowpipe", quantity = 1 },
	},
	["nightvision_scope"] = { 
		{item = "iron", quantity = 4 }, 
		{item = "blowpipe", quantity = 1 },
	},
	["advanced_scope"] = { 
		{item = "iron", quantity = 4 }, 
		{item = "blowpipe", quantity = 1 },
	},
	["super_torch"] = { 
		{item = "iron", quantity = 15 }, 
		{item = "blowpipe", quantity = 1 },
	},
	["clip"] = { 
		{item = "iron", quantity = 4 }, 
		{item = "blowpipe", quantity = 1 },
	},
	["scope"] = { 
		{item = "iron", quantity = 4 }, 
		{item = "blowpipe", quantity = 1 },
	},
	["silent"] = { 
		{item = "iron", quantity = 4 }, 
		{item = "blowpipe", quantity = 1 },
	},	
	-- armas --
	['WEAPON_ASSAULTRIFLE'] = { 
		{item = "iron", quantity = 100 }, 
		{item = "blowpipe", quantity = 2 },
		{item = "pepita_copper", quantity = 8 }, 
		{item = "stone", quantity = 5 },
	},
	['WEAPON_microsmg'] = { 
		{item = "iron", quantity = 38 }, 
		{item = "blowpipe", quantity = 2 },
		{item = "pepita_copper", quantity = 5 }, 
		{item = "stone", quantity = 3 },
	},
	['WEAPON_PUMPSHOTGUN'] = { 
		{item = "iron", quantity = 79 }, 
		{item = "blowpipe", quantity = 2 },
		{item = "pepita_copper", quantity = 7 }, 
		{item = "stone", quantity = 5 },
	},
	['WEAPON_CARBINERIFLE'] = { 
		{item = "iron", quantity = 125 }, 
		{item = "blowpipe", quantity = 3 },
		{item = "pepita_copper", quantity = 12 }, 
		{item = "stone", quantity = 6 },
	},
	['WEAPON_SPECIALCARBINE'] = { 
		{item = "iron", quantity = 175 }, 
		{item = "blowpipe", quantity = 4 },
		{item = "pepita_copper", quantity = 16 }, 
		{item = "stone", quantity = 10 },
	},
	['WEAPON_PISTOL50'] = { 
		{item = "iron", quantity = 25 }, 
		{item = "blowpipe", quantity = 1 },
		{item = "pepita_copper", quantity = 4 }, 
		{item = "stone", quantity = 3 },
	}
}

-- Enable a shop to access the crafting menu
Config.Shop = {
	useShop = false,
	shopCoordinates = { x=1088.358, y=-3101.302, z=-39.999 },
	shopName = "",
	shopBlipID = 446,
	zoneSize = { x = 2.5, y = 2.5, z = 1.5 },
	zoneColor = { r = 255, g = 0, b = 0, a = 100 }
}

-- Enable crafting menu through a keyboard shortcut
Config.Keyboard = {
	useKeyboard = false,
	keyCode = 303
}
