local bancoPeds = {
    {model= "A_C_Cow", voice="S_F_M_FEMBARBER_BLACK_MINI_01", x= 1447.60, y= 1152.78, z= 114.33-1.0001,  h=176.81 },
    {model= "A_C_Cow", voice="S_F_M_FEMBARBER_BLACK_MINI_01", x= 1458.63, y= 1141.74, z= 114.33-1.0001,  h=359.09},
    {model= "A_C_Cow", voice="S_F_M_FEMBARBER_BLACK_MINI_01", x=1469.40, y=1152.37, z=114.31-1.0001,  h=177.06},
    {model= "A_C_Cow", voice="S_F_M_FEMBARBER_BLACK_MINI_01", x=1469.35, y=1167.05, z=114.33-1.0001,  h=171.01},
    {model= "A_C_Cow", voice="S_F_M_FEMBARBER_BLACK_MINI_01", x=1447.52, y=1167.62, z=114.33-1.0001,  h=179.81},
    {model= "A_C_Cow", voice="S_F_M_FEMBARBER_BLACK_MINI_01", x=1441.41, y=1156.14, z=114.33-1.0001,  h=356.98}
}

Citizen.CreateThread(function()
	for k,v in ipairs(bancoPeds) do
		RequestModel(GetHashKey(v.model))
		while not HasModelLoaded(GetHashKey(v.model)) do
			Wait(0)
		end
		
        -- RequestAnimDict("mini@strip_club@idles@bouncer@base")
        -- while not HasAnimDictLoaded("mini@strip_club@idles@bouncer@base") do
          -- Wait(1)
        -- end

		local bancoPed = CreatePed(4, GetHashKey(v.model), v.x, v.y, v.z, v.a, false, false)
		SetBlockingOfNonTemporaryEvents(bancoPed, true)
		FreezeEntityPosition(bancoPed, true)
		SetEntityInvincible(bancoPed, true)
		SetAmbientVoiceName(bancoPed, v.voice)
		--TaskPlayAnim(bancoPed,"mini@strip_club@idles@bouncer@base","base", 8.0, 0.0, -1, 1, 0, 0, 0, 0)

		SetModelAsNoLongerNeeded(GetHashKey(v.model))
	end
end)
