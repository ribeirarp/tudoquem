Config                            = {}

Config.DrawDistance               = 100.0
Config.MarkerType                 = 27
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
Config.MarkerColor                = { r = 50, g = 50, b = 204 }

Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- enable if you're using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = true -- enable if you're using esx_license

Config.EnableHandcuffTimer        = true -- enable handcuff timer? will unrestrain player after the time ends
Config.HandcuffTimer              = 10 * 60000 -- 10 mins

Config.EnableJobBlip              = true -- enable blips for colleagues, requires esx_society

Config.MaxInService               = 20
Config.Locale                     = 'br'

Config.exercitoStations = {

  exercito1 = {

    Blip = {
      Pos     = { x = -446.84, y = 6012.7, z = 31.75 }, 
      Sprite  = 60,
      Display = 4,
      Scale   = 1.2,
      Colour  = 69,
    },

    AuthorizedWeapons = {
      { name = 'WEAPON_FLASHLIGHT',       price = 200 },	
      { name = 'WEAPON_NIGHTSTICK',       price = 400 },	
      { name = 'WEAPON_STUNGUN',          price = 3000 },	
	  { name = 'WEAPON_COMBATPISTOL',     price = 5000 }, 	
      { name = 'WEAPON_PUMPSHOTGUN',      price = 20000 }, 	
  	  { name = 'WEAPON_SMG',              price = 25000 },
      { name = 'WEAPON_CARBINERIFLE',     price = 30000 },
	  { name = 'WEAPON_SMOKEGRENADE',     price = 300 },
	  { name = 'WEAPON_BZGAS',            price = 300 },
	  { name = 'GADGET_PARACHUTE',        price = 500 },

    },

    Cloakrooms = {
      {x = -2350.29, y = 3266.03, z = 32.81-0.98}, 
      {x = 1818.64, y = 2594.26, z = 45.71-0.98}	  
    },

    Armories = {
      {x = -2358.38, y = 3255.11, z = 32.81-0.98},
      {x = 1791.59, y = 2593.70, z = 45.79-0.98}	  
    },

    Vehicles = {
      {
	  
	  
        Spawner    = {x = 472.84356689453, y = -1022.6272583008, z = 28.130533218384-0.98 },     
        SpawnPoints = {
			{ x = -2267.23, y = 3225.36, z = 32.81, heading = 237.6, radius = 8.0 },   
			{ x = -2251.38, y = 3248.77, z = 32.81, heading = 237.6, radius = 3.0 },  
			{ x = -2237.06, y = 3271.92, z = 32.81, heading = 237.6, radius = 3.0 }   
			
		}
    }
	
	
},

    Helicopters = {
      {
        Spawner    = {x = -450.56, y = 6009.88, z = 37.80-0.98},   
        SpawnPoint = {x = -474.77, y = 5989.08, z = 32.33-0.98},   
		Heading    = 320.0
      }
    },

    VehicleDeleters = {
      {x = -2311.96, y = 3273.50, z = 32.82-0.98}  
      --{x = 462.40, y = -1019.7, z = 27.115}
    },

    BossActions = {
      {x = -438.55, y =  6002.89, z = 36.80-0.98} 
    }

  }

}

-- https://wiki.rage.mp/index.php?title=Vehicles
Config.AuthorizedVehicles = {

    Shared = {
		
		{
			model = 'Mesa3',
			label = 'Jipe 4x4'
		},
		{
			model = 'Barracks',
			label = 'Transporte Pessoal'
		},
        {
			model = 'uparmorw',
			label = 'Blindado Armored'
		},
		{
			model = 'unarmed2',
			label = 'Blindado'
		},
		{
			model = 'Crusader',
			label = 'Jeep'
		}
		
	
	
	
	},
	
	





	
	praca = {
		

	},

	cabo = {
		

	},

	primeirosargento = {
		
		

	},

	sargentochefe = {
		
        

	},
	
	sargentomor = {
		
        

	},
	
	tenente = {
		
        

	},
	
	
	boss = {
		{
			model = 'exercito_i8',
			label = 'BMW I8'
		}
		
	}
}


-- CHECK SKINCHANGER CLIENT MAIN.LUA for matching elements

Config.Uniforms = {
	policia_wear = {
		male = {
			tshirt_1 = 58,
			tshirt_2 = 0,
			torso_1 = 26,
			torso_2 = 1,
			arms = 11,
			pants_1 = 31,
			pants_2 = 0,
			shoes_1 = 24,
			shoes_2 = 0,
			helmet_1 = 106,
			mask_1 = 0,
			mask_2 = 0,
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	guarda_wear = {
		male = {
			tshirt_1 = 58,
			tshirt_2 = 0,
			torso_1 = 55,
			torso_2 = 0,
			arms = 0,
			pants_1 = 65,
			pants_2 = 0,
			shoes_1 = 61,
			shoes_2 = 0,
			helmet_1 = -1
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	soldado_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	sargento_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 1,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 1,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	major_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 3,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 3,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	boss_wear = { -- currently the same as chef_wear
		male = {
			['tshirt_1'] = 130,  ['tshirt_2'] = 0,
			['torso_1'] = 53,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 11,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = 106,  ['helmet_2'] = 20,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	bullet_wear = {
		male = {
			['bproof_1'] = 11,  ['bproof_2'] = 0
		},
		female = {
			['bproof_1'] = 13,  ['bproof_2'] = 1
		}
	},
	teste_wear = {
		male = {
			tshirt_1 = 58,
			tshirt_2 = 0,
			torso_1 = 111,
			torso_2 = 0,
			arms = 55,
			pants_1 = 47,
			pants_2 = 0,
			shoes_1 = 51,
			shoes_2 = 0,
			helmet_1 = 124,
			helmet_2 = 0,
			mask_1 = 52,
			mask_2 = 0,
			bproof_1 = 11,
			bproof_2 = 1,
		
		},
		female = {
			['bproof_1'] = 13,  ['bproof_2'] = 1
		}
	},
	
	gilet_wear = {
		male = {
			['tshirt_1'] = 59,  ['tshirt_2'] = 1
		},
		female = {
			['tshirt_1'] = 36,  ['tshirt_2'] = 1
		}
	}
}