INSERT INTO `addon_account` (name, label, shared) VALUES
	('society_exercito', 'Exercito', 1)
;

INSERT INTO `datastore` (name, label, shared) VALUES
	('society_exercito', 'Exercito', 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
	('society_exercito', 'Exercito', 1)
;

INSERT INTO `jobs` (name, label) VALUES
	('exercito','Exercito')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
	('exercito',0,'praca',				'Praça',				  650,'{}','{}'),
	('exercito',1,'cabo',				'Cabo',   				  750,'{}','{}'),
	('exercito',2,'primeirosargento',	'Primeiro Sargento',	  850,'{}','{}'),
	('exercito',3,'sargentochefe', 		'Sargento Chefe',		  950,'{}','{}'),
	('exercito',4,'sargentomor',	    'Sargento Mor', 	     1200,'{}','{}'),
	('exercito',5,'tenente', 			'Tenente', 			     2000 ,'{}','{}'),
	('exercito',6,'boss',				'General', 			     3000,'{}','{}')
;

CREATE TABLE `fine_types` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`label` varchar(255) DEFAULT NULL,
	`amount` int(11) DEFAULT NULL,
	`category` int(11) DEFAULT NULL,

	PRIMARY KEY (`id`)
);

