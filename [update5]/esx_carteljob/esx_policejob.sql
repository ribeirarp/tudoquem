INSERT INTO `addon_account` (name, label, shared) VALUES
	('society_cartel', 'cartel', 1)
;

INSERT INTO `datastore` (name, label, shared) VALUES
	('society_cartel', 'cartel', 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
	('society_cartel', 'cartel', 1)
;

INSERT INTO `jobs` (name, label) VALUES
	('cartel','LSPD')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
	('cartel',0,'recruit','Recrue',20,'{}','{}'),
	('cartel',1,'officer','Officier',40,'{}','{}'),
	('cartel',2,'sergeant','Sergent',60,'{}','{}'),
	('cartel',3,'lieutenant','Lieutenant',85,'{}','{}'),
	('cartel',4,'boss','Commandant',100,'{}','{}')
;

CREATE TABLE `fine_types` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`label` varchar(255) DEFAULT NULL,
	`amount` int(11) DEFAULT NULL,
	`category` int(11) DEFAULT NULL,

	PRIMARY KEY (`id`)
);

