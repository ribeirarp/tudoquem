ESX                = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterCommand("Prisao", function(src, args, raw)

	local xPlayer = ESX.GetPlayerFromId(src)

	if xPlayer["job"]["name"] == "police" or xPlayer["job"]["name"] == "gnr" or xPlayer["job"]["name"] == "exercito" then

		local PrisaoPlayer = args[1]
		local PrisaoTime = tonumber(args[2])
		local PrisaoReason = args[3]

		if GetPlayerName(PrisaoPlayer) ~= nil then

			if PrisaoTime ~= nil then
				PrisaoPlayer(PrisaoPlayer, PrisaoTime)

				TriggerClientEvent("esx:showNotification", src, GetPlayerName(PrisaoPlayer) .. "  Preso por " .. PrisaoTime .. " minutos!")
				
				if args[3] ~= nil then
					GetRPName(PrisaoPlayer, function(Firstname, Lastname)
						TriggerClientEvent('chat:addMessage', -1, { args = { "JUIZ",  Firstname .. " " .. Lastname .. " Foi agora preso por: " .. args[3] }, color = { 249, 166, 0 } })
					end)
				end
			else
				TriggerClientEvent("esx:showNotification", src, "O tempo introduzido é inválido!")
			end
		else
			TriggerClientEvent("esx:showNotification", src, "Esse ID não está online!")
		end
	else
		TriggerClientEvent("esx:showNotification", src, "Não és um agente da autoridade!")
	end
end)

RegisterCommand("unPrisao", function(src, args)

	local xPlayer = ESX.GetPlayerFromId(src)

	if xPlayer["job"]["name"] == "police" then

		local PrisaoPlayer = args[1]

		if GetPlayerName(PrisaoPlayer) ~= nil then
			UnPrisao(PrisaoPlayer)
		else
			TriggerClientEvent("esx:showNotification", src, "Esse ID não está online!")
		end
	else
		TriggerClientEvent("esx:showNotification", src, "Não és um agente da autoridade!")
	end
end)

RegisterServerEvent("esx-qalle-Prisao:PrisaoPlayer")
AddEventHandler("esx-qalle-Prisao:PrisaoPlayer", function(targetSrc, PrisaoTime, PrisaoReason)
	local src = source
	local targetSrc = tonumber(targetSrc)

	PrisaoPlayer(targetSrc, PrisaoTime)

	GetRPName(targetSrc, function(Firstname, Lastname)
		TriggerClientEvent('chat:addMessage', -1, { args = { "JUIZ",  Firstname .. " " .. Lastname .. " Foi agora preso por: " .. args[3] }, color = { 249, 166, 0 } })
	end)
		TriggerClientEvent("esx:showNotification", src, GetPlayerName(PrisaoPlayer) .. "  Preso por " .. PrisaoTime .. " minutos!")
end)

RegisterServerEvent("esx-qalle-Prisao:unPrisaoPlayer")
AddEventHandler("esx-qalle-Prisao:unPrisaoPlayer", function(targetIdentifier)
	local src = source
	local xPlayer = ESX.GetPlayerFromIdentifier(targetIdentifier)

	if xPlayer ~= nil then
		UnPrisao(xPlayer.source)
	else
		MySQL.Async.execute(
			"UPDATE users SET Prisao = @newPrisaoTime WHERE identifier = @identifier",
			{
				['@identifier'] = targetIdentifier,
				['@newPrisaoTime'] = 0
			}
		)
	end

	TriggerClientEvent("esx:showNotification", src, xPlayer.name .. " UnPrisaoed!")
end)

RegisterServerEvent("esx-qalle-Prisao:updatePrisaoTime")
AddEventHandler("esx-qalle-Prisao:updatePrisaoTime", function(newPrisaoTime)
	local src = source

	EditPrisaoTime(src, newPrisaoTime)
end)

RegisterServerEvent("esx-qalle-Prisao:prisonWorkReward")
AddEventHandler("esx-qalle-Prisao:prisonWorkReward", function()
	local src = source

	local xPlayer = ESX.GetPlayerFromId(src)

	xPlayer.addMoney(math.random(13, 21))

	TriggerClientEvent("esx:showNotification", src, "Obrigado, tens aqui um pouco de dinheiro para a comida.")
end)

function PrisaoPlayer(PrisaoPlayer, PrisaoTime)
	TriggerClientEvent("esx-qalle-Prisao:PrisaoPlayer", PrisaoPlayer, PrisaoTime)

	EditPrisaoTime(PrisaoPlayer, PrisaoTime)
end

function UnPrisao(PrisaoPlayer)
	TriggerClientEvent("esx-qalle-Prisao:unPrisaoPlayer", PrisaoPlayer)

	EditPrisaoTime(PrisaoPlayer, 0)
end

function EditPrisaoTime(source, PrisaoTime)

	local src = source

	local xPlayer = ESX.GetPlayerFromId(src)
	local Identifier = xPlayer.identifier

	MySQL.Async.execute(
       "UPDATE users SET Prisao = @newPrisaoTime WHERE identifier = @identifier",
        {
			['@identifier'] = Identifier,
			['@newPrisaoTime'] = tonumber(PrisaoTime)
		}
	)
end

function GetRPName(playerId, data)
	local Identifier = ESX.GetPlayerFromId(playerId).identifier

	MySQL.Async.fetchAll("SELECT firstname, lastname FROM users WHERE identifier = @identifier", { ["@identifier"] = Identifier }, function(result)

		data(result[1].firstname, result[1].lastname)

	end)
end

ESX.RegisterServerCallback("esx-qalle-Prisao:retrievePrisaoedPlayers", function(source, cb)
	
	local PrisaoedPersons = {}

	MySQL.Async.fetchAll("SELECT firstname, lastname, Prisao, identifier FROM users WHERE Prisao > @Prisao", { ["@Prisao"] = 0 }, function(result)

		for i = 1, #result, 1 do
			table.insert(PrisaoedPersons, { name = result[i].firstname .. " " .. result[i].lastname, PrisaoTime = result[i].Prisao, identifier = result[i].identifier })
		end

		cb(PrisaoedPersons)
	end)
end)

ESX.RegisterServerCallback("esx-qalle-Prisao:retrievePrisaoTime", function(source, cb)

	local src = source

	local xPlayer = ESX.GetPlayerFromId(src)
	local Identifier = xPlayer.identifier


	MySQL.Async.fetchAll("SELECT Prisao FROM users WHERE identifier = @identifier", { ["@identifier"] = Identifier }, function(result)

		local PrisaoTime = tonumber(result[1].Prisao)

		if PrisaoTime > 0 then

			cb(true, PrisaoTime)
		else
			cb(false, 0)
		end

	end)
end)