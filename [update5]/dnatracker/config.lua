DnaTracker = {}
local MFD = DnaTracker
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj; end)

Citizen.CreateThread(function(...)
  while not ESX do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj; end)
    Citizen.Wait(0)
  end
end)

MFD.Version = '1.0.10'

MFD.BloodObject = "p_bloodsplat_s"
MFD.ResidueObject = "w_pi_flaregun_shell"

-- JOB Database Table: LABEL
MFD.PoliceJob = "PJ"
MFD.AmbulanceJob = "PJ"
  
MFD.DNAAnalyzePos = vector3(458.64660644531, -979.20874023438, 24.914710998535)
MFD.AmmoAnalyzePos = vector3(461.51055908203, -982.50341796875, 24.914710998535)
MFD.DrawTextDist = 3.0
MFD.MaxObjCount = 10