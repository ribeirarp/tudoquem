Config                            = {}

Config.DrawDistance               = 100.0
Config.MarkerType                 = 27
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
Config.MarkerColor                = { r = 50, g = 50, b = 204 }

Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- enable if you're using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = true -- enable if you're using esx_license

Config.EnableHandcuffTimer        = true -- enable handcuff timer? will unrestrain player after the time ends
Config.HandcuffTimer              = 10 * 60000 -- 10 mins

Config.EnableJobBlip              = true -- enable blips for colleagues, requires esx_society

Config.MaxInService               = 10
Config.Locale                     = 'br'
Config.LicensePrice				  = 25000


Config.PoliceStations = {

  LSPD1 = {

    Blip = {
      Pos     = { x = 425.130, y = -979.558, z = 30.711 },
      Sprite  = 60,
      Display = 4,
      Scale   = 1.2,
      Colour  = 29,
    },

    AuthorizedWeapons = {
      { name = 'WEAPON_FLASHLIGHT',       price = 200 },	
      { name = 'WEAPON_NIGHTSTICK',       price = 400 },	
      { name = 'WEAPON_STUNGUN',          price = 3000 },	
	  { name = 'WEAPON_COMBATPISTOL',     price = 5000 }, 	
      { name = 'WEAPON_PUMPSHOTGUN',      price = 20000 }, 	
  	  { name = 'WEAPON_SMG',              price = 25000 },
      { name = 'WEAPON_CARBINERIFLE',     price = 30000 },
	  { name = 'WEAPON_SMOKEGRENADE',     price = 300 },
	  { name = 'WEAPON_BZGAS',            price = 300 },
	  { name = 'GADGET_PARACHUTE',        price = 500 },

    },

    Cloakrooms = {
      {x = 452.600, y = -993.306, z = 29.750}
    },

    Armories = {
      {x = 451.699, y = -980.356, z = 29.693}
    },

    Vehicles = {
      {
        Spawner    = {x = 457.41, y = -1017.22, z = 28.33-0.98 },
        SpawnPoints = {
			{ x = 438.42, y = -1018.30, z = 27.75, heading = 90.0, radius = 6.0 },
			{ x = 441.08, y = -1024.23, z = 28.30, heading = 90.0, radius = 6.0 },
			{ x = 453.53, y = -1022.20, z = 28.02, heading = 90.0, radius = 6.0 },
			{ x = 450.97, y = -1016.55, z = 28.10, heading = 90.0, radius = 6.0 }
		}
    },
	
	  {
		Spawner    = { x = 473.38, y = -1018.43, z = 27.04 },
		SpawnPoints = {
			{ x = 475.98, y = -1021.65, z = 28.06, heading = 276.11, radius = 6.0 },
			{ x = 484.10, y = -1023.19, z = 27.57, heading = 302.54, radius = 6.0 }
		}
	}
},

    Helicopters = {
      {
        Spawner    = {x = 466.477, y = -982.819, z = 42.695},
        SpawnPoint = {x = 450.04, y = -981.14, z = 42.695},
		Heading    = 0.0
      }
    },

    VehicleDeleters = {
      {x = 462.74, y = -1014.4, z = 27.075},
      {x = 462.40, y = -1019.7, z = 27.115},
	  {x = 472.84356689453, y = -1022.6272583008, z = 28.130533218384}
	    

    },

    BossActions = {
      {x = 448.417, y = -973.208, z = 29.693}
    }

  }

}

-- https://wiki.rage.mp/index.php?title=Vehicles
Config.AuthorizedVehicles = {

    Shared = {
		{
			model = 'psp_bmwgs',
			label = 'Mota'
		},
		{
			model = 'dgrsp_vwcrafter',
			label = 'Carrinha de prisioneiros'
		},
		{
			model = 'riot',
			label = 'Blindado'
		},
		{
			model = 'pspt_els',
			label = 'Carrinha de intervenção'
		},
		{
			model = 'pspp_530d',
			label = 'BMW 530D'
		},
		{
			model = 'police2',
			label = 'Carrinha Ford'
		}
		
	
	
	
	},
	
	recruit = {
		

	},

	officer = {


		{
			model = 'sheriffsci',
			label = 'Scirocco descaracterizado'
		}

	},

	sergeant = {

		{
			model = 'sheriffsci',
			label = 'Scirocco descaracterizado'
		}
		

	},

	lieutenant = {

		{
			model = 'sheriffsci',
			label = 'Scirocco descaracterizado'
		},
        {
			model = 'policer8',
			label = 'Audi R8'
		}

	},
	
	boss1 = {
		
		{
			model = 'sheriffsci',
			label = 'Scirocco descaracterizado'
		},
        {
			model = 'policer8',
			label = 'Audi R8'
		},
		
		
	},
	
	headchief = {
		
		{
			model = 'sheriffsci',
			label = 'Scirocco descaracterizado'
		},
        {
			model = 'policer8',
			label = 'Audi R8'
		},
		
		
	},
	
	chiefcoordinator = {
		
		{
			model = 'sheriffsci',
			label = 'Scirocco descaracterizado'
		},
        {
			model = 'policer8',
			label = 'Audi R8'
		},
		
		
	},
	boss = {
		
		{
			model = 'sheriffsci',
			label = 'Scirocco descaracterizado'
		},
        {
			model = 'policer8',
			label = 'Audi R8'
		},
		
		
	}
}



-- CHECK SKINCHANGER CLIENT MAIN.LUA for matching elements

Config.Uniforms = {
	policia_wear = {
		male = {
			tshirt_1 = 58,
			tshirt_2 = 0,
			torso_1 = 55,
			torso_2 = 0,
			arms = 0,
			pants_1 = 65,
			pants_2 = 0,
			shoes_1 = 61,
			shoes_2 = 0,
			helmet_1 = -1,
			mask_1 = 0,
			mask_2 = 0,
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	guarda_wear = {
		male = {
			tshirt_1 = 58,
			tshirt_2 = 0,
			torso_1 = 55,
			torso_2 = 0,
			arms = 0,
			pants_1 = 65,
			pants_2 = 0,
			shoes_1 = 61,
			shoes_2 = 0,
			helmet_1 = -1
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	soldado_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	sargento_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 1,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 1,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	major_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 3,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 3,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	boss_wear = { -- currently the same as chef_wear
		male = {
			['tshirt_1'] = 130,  ['tshirt_2'] = 0,
			['torso_1'] = 53,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 11,
			['pants_1'] = 31,   ['pants_2'] = 0,
			['shoes_1'] = 24,   ['shoes_2'] = 0,
			['helmet_1'] = 106,  ['helmet_2'] = 20,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	bullet_wear = {
		male = {
			['bproof_1'] = 11,  ['bproof_2'] = 2
		},
		female = {
			['bproof_1'] = 13,  ['bproof_2'] = 1
		}
	},
	gilet_wear = {
		male = {
			['tshirt_1'] = 59,  ['tshirt_2'] = 1
		},
		female = {
			['tshirt_1'] = 36,  ['tshirt_2'] = 1
		}
	}
}