INSERT INTO `addon_account` (name, label, shared) VALUES
	('society_pj', 'PJ', 1)
;

INSERT INTO `datastore` (name, label, shared) VALUES
	('society_pj', 'PJ', 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
	('society_pj', 'PJ', 1)
;

INSERT INTO `jobs` (name, label) VALUES
	('pj','PJ')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
	('pj',0,'recruit','Estagiário',1200,'{}','{}'),
	('pj',1,'officer','Inspetor',1400,'{}','{}'),
	('pj',2,'sergeant','Inspetor Chefe',1600,'{}','{}'),
	('pj',3,'lieutenant','Coordenador',1800,'{}','{}'),
	('pj',4,'boss','Diretor',2000,'{}','{}')
;

CREATE TABLE `fine_types` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`label` varchar(255) DEFAULT NULL,
	`amount` int(11) DEFAULT NULL,
	`category` int(11) DEFAULT NULL,

	PRIMARY KEY (`id`)
);

