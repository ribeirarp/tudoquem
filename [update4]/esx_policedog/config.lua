-- TriggerEvent('esx_policedog:openMenu') to open menu

Config = {
    Job = 'police',
	Job2 = 'gnr',
	Job3 = 'pj',
    Command = 'max', -- set to false if you dont want to have a command
    Model = 351016938,
    TpDistance = 50.0,
    Sit = {
        dict = 'creatures@rottweiler@amb@world_dog_sitting@base',
        anim = 'base'
    },
    Drugs = {'cannabis', 'marijuana', 'coca', 'cocaine', 'ephedra', 'ephedrine', 'poppy', 'opium'}, -- add all drugs here for the dog to detect
}

Strings = {
    ['not_police'] = 'Tu ~r~não ~s~és um polícia!',
    ['menu_title'] = 'Max',
    ['take_out_remove'] = 'Fazer Aparecer / desaparecer o Max',
    ['deleted_dog'] = 'O Max foi para casa',
    ['spawned_dog'] = 'O Max apareceu',
    ['sit_stand'] = 'Fazer o Max sentar / leventar',
    ['no_dog'] = "Não tens nenhum cão!",
    ['dog_dead'] = 'O Max está morto',
    ['search_drugs'] = 'Fazer o Max cheirar a pessoa mais próxima',
    ['no_drugs'] = 'O Max não encontrou nenhuma droga.', 
    ['drugs_found'] = 'O Max encontrou drogas',
    ['dog_too_far'] = 'O Max está muito longe!',
    ['attack_closest'] = 'Atacar a pessoa mais perto',
    ['get_in_out'] = 'Fazer o Max entrar / sair do veículo'
}