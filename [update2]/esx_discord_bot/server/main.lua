ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

--Send the message to your discord server
function sendToDiscord (source,target,message,msgType,steamIdentifier)
  local DiscordWebHook = Config.webhook
  local color = 0

  print(msgType)
 
  if (msgType == "anon") then
		DiscordWebHook = Config.webhookAnon
		color = Config.red
  elseif (msgType == "twt") then
		DiscordWebHook = Config.webhookTwt
		color = Config.bluetweet
  elseif (msgType == "ooc") then
		DiscordWebHook = Config.webhookOoc
		color = Config.grey
  elseif (msgType == "giveweapon") then
		DiscordWebHook = Config.webhookGiveweapon
		color = Config.pink
  elseif (msgType == "setmoney") then
		DiscordWebHook = Config.webhookSetmoney
		color = Config.green
  elseif (msgType == "giveitem") then
		DiscordWebHook = Config.webhookGiveitem
		color = Config.grey
  elseif (msgType == "mala") then
		DiscordWebHook = Config.webhookMala
		color = Config.grey
  elseif (msgType == "empresas") then
		DiscordWebHook = Config.webhookEmpresas
		color = Config.grey
  end
 
  local embeds = {}
  if msgType == "giveweapon" or msgType == "setmoney" or msgType == "giveitem" then
		 embeds = {
		{
			["title"]= "Discord Logs",
			["type"]= "rich",
			["color"] = color,
			['fields']= {{
					['name'] = "***Staff***",
					['value'] = source
				},
				{
					['name'] = "***Target***",
					['value'] = target
				
				},
				{
					['name'] = "***Deu***",
					['value'] = message
				
				},
			
			},
			["footer"]=  {
				["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
		   },
		}
	  }
  elseif msgType == "mala" then
		embeds = {
		{
			["title"]= "Discord Logs",
			["type"]= "rich",
			["color"] = color,
			['fields']= {{
					['name'] = "***Matricula***",
					['value'] = source
				},
				{
					['name'] = "***Dono***",
					['value'] = steamIdentifier
				
				},
				{
					['name'] = "***Quem fez a transação***",
					['value'] = target
				
				},
				{
					['name'] = "***Conteudo***",
					['value'] = message
				
				},
			
			},
			["footer"]=  {
				["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
		   },
		}
	  }
	elseif msgType == "empresas" then
		embeds = {
			{
				["title"]= "Discord Logs",
				["type"]= "rich",
				["color"] = color,
				['fields']= {{
						['name'] = "***Sociedade***",
						['value'] = source
					},
					{
						['name'] = "***Quem fez***",
						['value'] = steamIdentifier
					
					},
					{
						['name'] = "***Conteudo***",
						['value'] = message
					
					},
				
				},
				["footer"]=  {
					["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
				},
			}
	}
  else
	  embeds = {
		{
			["title"]= "Discord Chat Logs",
			["type"]= "rich",
			["color"] = color,
			['fields']= {{
					['name'] = "***ID | Nome***",
					['value'] = source
				},
				{
					['name'] = "***Mensagem***",
					['value'] = message
				
				},
				{
					['name'] = "***Steam ID***",
					['value'] = target
				
				},
			
			},
			["footer"]=  {
				["text"]= os.date('%A, %B %d %Y at %I:%M:%S %p'),
		   },
		}
	  }
  end

  if message == nil or message == '' then return FALSE end
  PerformHttpRequest(DiscordWebHook, function(err, text, headers) end, 'POST', json.encode({ username = name,embeds = embeds}), { ['Content-Type'] = 'application/json' })
end


-- Event when a player is connecting
RegisterServerEvent("esx_discord_bot:mandar")
AddEventHandler('esx_discord_bot:mandar', function(source, target, message, msgType, steamIdentifier)
    sendToDiscord(source,target,message,msgType,steamIdentifier)
end)

function stringsplit(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={} ; i=1
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end

function getIdentity(source)
    local identifier = GetPlayerIdentifiers(source)[1]
    --local result = MySQL.Sync.fetchAll("SELECT * FROM characters WHERE identifier = @identifier", {
	local result = MySQL.Sync.fetchAll("SELECT * FROM users WHERE identifier = @identifier", {
        ['@identifier'] = identifier
    })
  if result[1] ~= nil then
    local identity = result[1]

    return {
      firstname   = identity['firstname'],
      lastname  = identity['lastname'],
      dateofbirth = identity['dateofbirth'],
      sex   = identity['sex'],
      height    = identity['height']
    }
  else
    return {
      firstname   = '',
      lastname  = '',
      dateofbirth = '',
      sex   = '',
      height    = ''
    }
    end
end