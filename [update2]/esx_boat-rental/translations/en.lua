Locales['en'] = {
	['press_e'] = 'clica ~INPUT_CONTEXT~ para alugares um barco.',
	['storebike'] = 'Clica ~INPUT_CONTEXT~ para guardares o barco.',	
	['biketitle'] = 'Aluguer de barcos',
	['bike'] = '<span style="color:green;">Tug</span> <span style="color:red;">400€</span>',
	['bike2'] = '<span style="color:green;">Mota de água</span> <span style="color:red;">600€</span>',
	['bike3'] = '<span style="color:green;">Barco</span> <span style="color:red;">750€</span>',
	['bike4'] = '<span style="color:green;">BMX</span> <span style="color:red;">300€</span>',
	['bikefree'] = 'Bike - <span style="color:green;">TriBike</span>',
	['bike2free'] = 'Bike - <span style="color:green;">Scorcher</span>',
	['bike3free'] = 'Bike - <span style="color:green;">Cruiser</span>',
	['bike4free'] = 'Bike - <span style="color:green;">BMX</span>',
	['bikemessage'] = 'Espero que tenhas gostado da viagem! Volta mais logo :)',
	['notabike'] = 'Nao estas num barco!',
	
	['bike_pay'] = 'Tu pagas-te: %s€',
	
	['bikes'] = '[Bikes]',
	
	['civil_outfit'] = '<span style="color:yellow;">Citizen Outfit</span>',
	['outfit'] = '<span style="color:green;">Bike Outfit</span> <span style="color:red;">200$</span>',
	['outfit2'] = '<span style="color:green;">Bike Outfit 2</span> <span style="color:red;">200$</span>',
	['outfit3'] = '<span style="color:green;">Bike Outfit 3</span> <span style="color:red;">200$</span>',
}