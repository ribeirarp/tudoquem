Config                            = {}
Config.Locale = 'en'


Config.Volume = 0.5 				-- 0.1 , 1.0
Config.EnablePrice = true -- false = bikes for free
Config.EnableEffects = true
Config.EnableSoundEffects = false
Config.EnableBlips = true

	
Config.PriceTriBike = 400
Config.PriceScorcher = 600
Config.PriceCruiser = 750
Config.PriceBmx = 1000

Config.EnableBuyOutfits = false -- WIP !!!!
	
Config.MarkerZones = { 
      {x = -1248.44,y = -1871.14,z = 0.50},
      
}


-- Edit blip titles
Config.BlipZones = { 
   {title="Aluguer de barcos", colour=2, id=455, x = -1248.44, y = -1871.14, z = 1.15},

}