Locales['en'] = {
  ['prompt_wash'] = 'Pressiona ~INPUT_CONTEXT~ para lavar este veículo',
  ['prompt_wash_paid'] = 'Pressiona ~INPUT_CONTEXT~ para lavar o veículo por ~g~%s€~s~',
  ['wash_failed'] = 'Não tens dinheiro para lavar o carro',
  ['wash_failed_clean'] = 'O teu veículo não precisa de uma lavagem.',
  ['wash_successful'] = 'O teu carro foi lavado!',
  ['wash_successful_paid'] = 'O teu veículo foi lavado por ~g~%s€~s~',
  ['blip_carwash'] = 'Lavagem de Carros',
}
